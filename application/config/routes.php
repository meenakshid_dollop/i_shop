<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'DefaultController';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/*****************Admin API********************/



////////user

$route['user_login']='webService/customer/UserController/user_login';
$route['match_otp']= 'webService/customer/UserController/match_otp';
$route['resend_otp']= 'webService/customer/UserController/resend_otp';
$route['user_signup']='webService/customer/UserController/user_signup';
$route['contactUs'] = 'webService/customer/ContactUs/contactUs';

$route['user_edit_profile']= 'webService/customer/UserController/user_edit_profile';
$route['get_user_profile']= 'webService/customer/UserController/getUserProfile';
////////////////////////request 
$route['addProductAndRequest'] = 'webService/customer/RequestController/addRequest';
$route['getAllRequestAndProduct'] = 'webService/customer/RequestController/getAllRequest';
$route['getAllRequestAndProduct/(:any)'] = 'webService/customer/RequestController/getAllRequest/$1';
/////////////////////////////bid

$route['getAllBid/(:any)'] = 'webService/customer/BidController/getAllBid/$1';
$route['getUserBid'] = 'webService/customer/BidController/getUserBid';
$route['placeBid'] = 'webService/customer/BidController/placeBid';
$route['UpdateBidStatus'] = 'webService/customer/BidController/UpdateBidStatus';
/////////////cateogry
$route['getSubCategoriesByUser/(:any)'] = 'webService/customer/UserCategory/getSubCategoriesByUser/$1';

///////////////////////category
$route['categories'] = 'webService/admin/CategoryController/Categories';
$route['getCategories/(:any)'] = 'webService/admin/CategoryController/getCategories/$1';
$route['getCategories'] = 'webService/admin/CategoryController/getCategories';
//////////////////////////subcategory
$route['subCategories'] = 'webService/admin/Subcategory/subCategories';
$route['getSubCategories/(:any)'] = 'webService/admin/subcategory/getSubCategories/$1';
$route['getSubCategories'] = 'webService/admin/subcategory/getSubCategories';
//////////////////referral code
//////////////////////////request

$route['getReferralCode'] = 'webService/admin/ReferralcodeController/getReferralCode';
$route['getReferralCode/(:any)'] = 'webService/admin/ReferralcodeController/getReferralCode/$1';
$route['referralCode'] = 'webService/admin/ReferralcodeController/referralCode';
$route['addCoupon'] = 'webService/admin/CouponController/addCoupon';
$route['getCoupon'] = 'webService/admin/CouponController/getCoupon';
$route['getCoupon/(:any)'] = 'webService/admin/CouponController/getCoupon/$1';
//////////////admin login
$route['admin_login'] = 'webService/admin/AdminAuth/admin_login';
$route['getUserByAdmin'] = 'webService/admin/UserData/getUserByAdmin';
$route['getUserByAdmin/(:any)'] = 'webService/admin/UserData/getUserByAdmin/$1';
$route['updateUserByAdmin/(:any)'] = 'webService/admin/UserData/updateUserByAdmin/$1';
///////////////////////terms and policy and faq module
$route['updatePolicy'] = 'webService/admin/TermsAndPolicyController/updatePolicy';
$route['getPolicy'] = 'webService/admin/TermsAndPolicyController/getPolicy';
$route['updateTermsAndPolicy'] = 'webService/admin/TermsAndPolicyController/updateTermsAndPolicy';
$route['getTermsAndCondition'] = 'webService/admin/TermsAndPolicyController/getTermsAndCondition';
$route['addFAQByAdmin'] = 'webService/admin/TermsAndPolicyController/addFAQByAdmin';
$route['getFaq'] = 'webService/admin/TermsAndPolicyController/getFaq';
$route['getFaq/(:any)'] = 'webService/admin/TermsAndPolicyController/getFaq/$1';
$route['deleteFaq/(:any)'] = 'webService/admin/TermsAndPolicyController/deleteFaq/$1';
$route['change_admin_password'] = 'webService/CommonController/change_password';

///////////////////////////////bid








///////////////////////////////////////admin page
$route['category'] = 'DefaultController/category';
$route['login'] = 'DefaultController/login';
$route['view_category'] = 'DefaultController/view_category';
$route['subcategory'] = 'DefaultController/subcategory';
$route['view_subcategory'] = 'DefaultController/view_subcategory';
$route['referral_code'] = 'DefaultController/referral_code';
$route['view_referral_code'] = 'DefaultController/view_referral_code';
$route['logout'] = 'DefaultController/logout';
$route['add_coupon'] = 'DefaultController/add_coupon';
$route['view_coupon'] = 'DefaultController/view_coupon';
$route['view_user'] = 'DefaultController/view_user';
$route['view_privacy'] = 'DefaultController/view_privacy';
$route['privacy_policy'] = 'DefaultController/privacy_policy';
$route['view_terms_condition'] = 'DefaultController/view_terms_condition';
$route['terms_and_condition'] = 'DefaultController/terms_and_condition';
$route['view_faq'] = 'DefaultController/view_faq';
$route['faq'] = 'DefaultController/faq';
$route['change_password'] = 'DefaultController/change_password';
$route['product_list'] = 'DefaultController/product_list';
$route['request_list'] = 'DefaultController/request_list';






