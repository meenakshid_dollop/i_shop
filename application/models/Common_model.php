<?php

Class Common_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library('email');
	}

	public function executeSql($sql,$type)
	{
		$query = $this->db->query($sql);
		switch ($type) {
		    case 'insert':
				$result = $query;
				break;
				
			case 'update':
				$result = $query;
				break;
			
			case 'delete':
				$result = $query;
				break;
			case 'result_array':
				$result = $query->result_array();
				break;
			case 'row_array':
				$result = $query->row_array();
				break;			
			case 'num_rows':
				$result = $query->num_rows();
				break;			
			default:
				$result = 'Failed';
				break;
		}		
				
		return $result;
	}

	public function insertData($table,$data)
	{
		$query = $this->db->insert($table,$data);
		if ($query)
		{
			return $this->db->insert_id();
		}
		else
		{
			return "0";
		}

	}

	public function getAllData($table,$key="")
	{
		if (isset($key)&&!empty($key)) {
			$this->db->order_by($key,"desc");
		}

		$query = $this->db->get($table);
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return 0;
		}
	}

	public function check_mobile($user_id,$phone)
	{
		$query = $this->db->select("*")
		->from("Users")
		->where("phone",$phone)
		->where_not_in('user_id', $user_id)
		->get();
		return ($query->num_rows() > 0) ? $query->row_array() : 0;
	}

	//mkr
	public function check_email_id($user_id,$email_id)
	{
		$query = $this->db->select("*")
		->from("Users")
		->where("email_id",$email_id)
		->where_not_in('user_id', $user_id)
		->get();
		return ($query->num_rows() > 0) ? $query->row_array() : 0;
	}

	//mkr
	public function getDataWhere($table,$select,$where)
	{
		$query = $this->db->select($select)->from($table)->where($where)->get();
		return ($query->num_rows() > 0) ? $query->row_array() : 0;
	}

	//mkr
	public function getAllDataWhere($table,$select,$where,$key="")
	{
		
		
		$this->db->select($select);
		$this->db->from($table);
		$this->db->where($where);
		if (isset($key)&&!empty($key)) {
			$this->db->order_by($key,"desc");
		}
		$query = $this->db->get();
		return ($query->num_rows() > 0) ? $query->result_array() : 0;
	}





	public function getAllDataByCondition($table,$cond){
		$query = $this->db->get_where($table,$cond);
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return 0;
		}
	}

	public function upload_image($image_data,$num,$path1)
	{
		$image = md5(date("d-m-y:h:i s"))."_".$num;
		if(is_array($image_data)) 
		{
			$file_name = pathinfo(@$image_data['name'], PATHINFO_FILENAME);
			$extension = pathinfo(@$image_data['name'], PATHINFO_EXTENSION);  

			if(move_uploaded_file(@$image_data['tmp_name'], $path1.''. $image.'.'.$extension)){
				$image = $image.'.'.$extension;

			}else{
				$image = Null;
			}

		}
		return $image;
	}

	public function upload_compress_image($image_data,$path,$quality,$num)
	{
		//$quality=80;
		$extension = pathinfo(@$image_data['name'], PATHINFO_EXTENSION);

		$image_name = md5(date("d-m-y:h:i s"))."_".$num.".".$extension;

		$info = getimagesize($image_data['tmp_name']);
        if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($image_data['tmp_name']);
        elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($image_data['tmp_name']);
        elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($image_data['tmp_name']);
        if(imagejpeg($image, $path.$image_name, $quality)){
        	$image_name = $image_name;
        }else{
			$image_name = Null;
		}
		
		return $image_name;
	}

	public function updateData($table, $cond, $data)
	{
		$this->db->where($cond);
		$res=$this->db->update($table, $data); 
		if($res)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	public function deleteData($table, $cond)
	{
		$this->db->where($cond);
		$res=$this->db->delete($table); 
		return $this->db->last_query();

		if($res)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

	public function deleteDataByCondition($table, $id)
	{
		$query = $this->db->delete($table,$id);
		if ($query)
		{
			return "1";
		}
		else
		{
			return "0";
		}
	}

	public function deleteDataWithImg($table, $id, $image)
	{
		if($image!='')
		{
			unlink($path.$image);
		}
		$query = $this->db->delete($table,$id);
		if ($query)
		{
			return "1";
		}
		else
		{
			return "0";
		}
	}
	public function getById($table,$where)
	{
		$query = $this->db->get_where($table,$where);
		return ($query->num_rows() > 0)?$query->row_array():false;
	}

	public function send_mail($sender, $reciever, $mail_message, $mail_subject ) 
	{ 
		
		$this->email->from($sender); 
		$this->email->to($reciever);
		$this->email->subject($mail_subject); 
		$this->email->message($mail_message); 

         //Send mail 
		if($this->email->send()) 
			return 1; 
		else 
			return 0; 
	}
	

	public function sendOtp($otp, $mobile_no,$msg)
	{

		//Your authentication key faba412fde694067947f25c7eaebee17
		$authKey = "543122ccd38d16e78456";
			//Multiple mobiles numbers separated by comma
		$mobileNumber = $mobile_no;
			//Sender ID,While using route4 sender id should be 6 characters long.
		$senderId = "PlDA7856";
			//Your message to send, Add URL encoding here.
		$message = $otp.$msg;
			//Define route 
		$route = "template";
			//$route = "default";
			//Prepare you post parameters 
		$postData = array(
			'authkey' => $authKey,
			'mobiles' => $mobileNumber,
			'message' => $message,
			'sender' => $senderId,
			'route' => $route
		);
			//API URL
		$url = "http://sms.bulksmsserviceproviders.com/api/send_http.php";
			// init the resource
		$ch = curl_init();
		curl_setopt_array($ch, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => $postData
	    		//,CURLOPT_FOLLOWLOCATION => true
		));

			//Ignore SSL certificate verification
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			//get response
		$output = curl_exec($ch);


		if ($output!="") {
			curl_close($ch);
			return 1;
		}
		else{
			curl_close($ch);
			return 0;
		}


			//echo $output;
	}

	public function sendDukaSms($mobile,$msg)
	{
		$ch = curl_init();
		/*$headers  = [
		            'x-api-key: hv70eajjBdSM83AUd1B2Si1mcO1wsGgck2baoPA66iw=',
		            'Content-Type: application/json'
		        ];*/
		/*if(strlen($mobile_no) < 13){
			$mobile_no = "964".$mobile_no;
		}*/
		$postData = [
		    'tel' => $mobile,
			'message' => $msg,
		];
		curl_setopt($ch, CURLOPT_URL,"https://inveli-pay.invely.co.ke/api/sms/send");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));           
		//curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$result     = curl_exec ($ch);
		$statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		//print_r($result);
	}

	public function getOtp($otp, $contact_no, $msg)
	{

		$result=array();
		$result['status']="1";
		$result['message']="Failed";
		$result['data']="";
		$current_date_time=date("Y-m-d h:i:s");

		$six_digit_otp = $otp;
		$query = $this->db->get_where("Users_otp",array("phone"=>$contact_no));

		if($query->num_rows() == 0)
		{ 
			$data=array("phone"=>$contact_no,"otp"=>$six_digit_otp,"modify_date"=>$current_date_time);     	
			$res=$this->db->insert("Users_otp", $data);
			if($res)
			{
				$msg=" is your Mobile verification OTP.";
				$is_send=$this->sendOtp($six_digit_otp, $contact_no,$msg);
				$result['status']="200";
				$result['message']="success";
				$result['data']=$six_digit_otp;
			}	
			else
			{
				$result['message']="Failed";
			}

		}
		else if($query->num_rows() > 0)
		{

			$date1 = strtotime($current_date_time);
			$date2 = strtotime($query->row_array()['modify_date']);	
			$diff = $date1 - $date2;

			if ($diff>20) {
				$permit_send=1;
				$data=array("otp"=>$six_digit_otp,"modify_date"=>$current_date_time);  
			} else {
				$permit_send=0;		
				$data=array("modify_date"=>$current_date_time); 
			}

			$this->db->where("phone", $contact_no);   	
			$res=$this->db->update("Users_otp", $data);		
			if($res)
			{
				if ($permit_send==1) {
					$msg=" is your Mobile verification OTP.";
					$is_send=$this->sendOtp($six_digit_otp, $contact_no,$msg);
				} else {
					$is_send=1;
					$six_digit_otp=$query->row_array()['otp'];
				}

				if ($is_send) {
					$result['status']="200";
					$result['message']="success";
					$result['data']=$six_digit_otp;
				} else {
					$result['status']="1";
					$result['message']="Failed";

				}


			}	
			else
			{
				$result['message']="Failed";
			}
		}
		else
		{
			$result['message']="Mobile Number Already Exist";
		}
		return $result;
	} 	

	public function getOtp_forgot_password($contact_no)
	{
		$result=array();
		$result['status']="1";
		$result['message']="Failed";
		$result['data']="";

		$six_digit_otp = mt_rand(100000, 999999);
		$query = $this->db->get_where("Users_otp",array("phone"=>$contact_no));
		if($query->num_rows() > 0)
		{ 
			$user_id=$query->row_array()['user_id'];
			$data=array("otp"=>$six_digit_otp)  ;  
			$this->db->where("phone", $contact_no);   	
			$res=$this->db->update("Users_otp", $data);
			if($res)
			{
				$msg=" is your Mobile verification OTP.";
				$is_send=$this->sendOtp($six_digit_otp, $contact_no,$msg);
				$result['status']="200";
				$result['message']="success";
				$result['data']=$six_digit_otp;
			}	
			else
			{
				$result['message']="Failed";
			}

		}
		else
		{
			$result['message']="Mobile Number Already Exist";
		}
		return $result;
	} 	

	public function matchToken($token)
	{
		$query = $this->db->get_where("Users",array("token"=>$token));
		if ($query->num_rows() > 0)
		{
			return $query->row_array()['user_id'];
		}
		else
		{
			return "0";
		}

	}	

    public function getAllDataWhereLimit($table,$select,$where,$key="",$off_set=0,$limit=10)
	{
		
		
		$this->db->select($select);
		$this->db->from($table);
		$this->db->where($where);
		if (isset($key)&&!empty($key)) {
			$this->db->order_by($key,"desc");
		}
		$this->db->limit($limit,$off_set); 
		$query = $this->db->get();
		return ($query->num_rows() > 0) ? $query->result_array() : 0;
	}
    

	public function distance($lat1, $lon1, $lat2, $lon2, $unit) 
	{
		$theta = $lon1 -$lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);

		if ($unit == "K") 
		{
			return ($miles * 1.609344);
		}
		else if ($unit == "N") 
		{
			return ($miles * 0.8684);
		}
		else
		{
			return $miles;
		}
	}


    // push notification 
	public function push_notification($message,$sender,$fcm_reg_id,$user_type,$action)
	{


		$payload = array();
		$payload['team'] = 'India';
		$payload['score'] = '5.6';

        // notification title

		$title = 'Care App';


        // push type - single user / topic
        //$push_type = "this is test";//isset($_GET['push_type']) ? $_GET['push_type'] : '';

        // whether to include to image or not
		$include_image = isset($_GET['include_image']) ? TRUE : FALSE;

		$this->push->setUsertype($user_type);
		$this->push->setAction($action);
		$this->push->setTitle($title);
		$this->push->setMessage($message);
		$this->push->setSender($sender);
echo "hello"; die;

		if ($include_image) {
			$this->push->setImage('http://api.androidhive.info/images/minion.jpg');
		} else {
			$this->push->setImage('');
		}
		$this->push->setIsBackground(FALSE);
		$this->push->setPayload($payload);

		$json = '';
		$response = '';

		$json = $this->push->getPush();
		$regId = $fcm_reg_id;

		$response = $this->firebase->send($regId, $json);
		print_r($response);

	}



	//solve null problem for single array like array("abc"=>1)
	public function getRowArray($array)
	{ 
		$localArray=array();
		if (!empty($array)) {
			foreach ($array as $key => $value) {
				if ($value=="") {
					$localArray[$key]="";
				}else{
					$localArray[$key]=$value;
				}
			}
		}
		return $localArray;
	}

	//solve null problem for multidimentional array 
	public function getResultArray($array)
	{
		$globleArray=array();
		if (!empty($array)) {
			foreach ($array as $key => $value) {
				$localArray=$this->getRowArray($value);
				array_push($globleArray, $localArray);
			}
		}
		return $globleArray;
	}


	public function get_user_data($user_id)
	{
		$query = $this->db->select('*')
				->from('Users')
				->join('City','City.city_id=Users.city_id','left')
				->where('user_id',$user_id)
				->get();
		return ($query->num_rows() > 0) ? $query->row_array() : 0;
	}

	public function createToken($check_user)
	{
		$token = md5(date("Y-m-d h:i:s").uniqid(rand(100000000, 999999999)));
		$where = array("user_id"=>$check_user['user_id']); 
		$change_data = $this->updateData("Users",$where,array('token'=>$check_user['user_id'].'_'.$token));
		if ($change_data) {
			return $token;
		} else {
			return 0;
		}		
	}

	public function deleteRowPreviousDate($table,$column,$date){
		$query = $this->db->query("delete from  $table  where  $column < '$date'");
	}

	function get_data_join_two_all($onetable,$twotable,$idone,$idtwo,$where)
	{ 
		$this->db->select('*');
		$this->db->from($onetable);
		$this->db->join($twotable, $twotable.'.'.$idone.'='.$onetable.'.'.$idtwo);
		$this->db->where($where);		
		$query = $this->db->get();
		return ($query->num_rows() > 0) ? $query->result_array() : 0;
	}


	function get_data_join_two_all_select($onetable,$twotable,$idone,$idtwo,$select,$where)
	{ 
		$this->db->select($select);
		$this->db->from($onetable);
		$this->db->join($twotable, $twotable.'.'.$idone.'='.$onetable.'.'.$idtwo);
		$this->db->where($where);
		$query = $this->db->get();
		return ($query->num_rows() > 0) ? $query->result_array() : 0;
	}

	function get_data_join_two_all_select_desc($onetable,$twotable,$idone,$idtwo,$select,$where,$order_by)
	{ 
		$this->db->select($select);
		$this->db->from($onetable);
		$this->db->join($twotable, $twotable.'.'.$idone.'='.$onetable.'.'.$idtwo);
		$this->db->where($where);
		$this->db->order_by($order_by,'desc');
		$query = $this->db->get();
		return ($query->num_rows() > 0) ? $query->result_array() : 0;
	}


	public function get_unique_code(){
		$length = 6;
		$password = "";
		$possible = "0123456789abcdfghjkmnpqrstvwxyz";
		$i = 0;
		while ($i < $length) {
			$char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
			if (!strstr($password, $char)) {
				$password .= $char;
				$i++;
			}
		} 
		return $refer_code =strtoupper($password);
	}
	public function send_mail_data($data,$data2)
	{

		$this->db->insert('Z_mail',$data);
		$id_table1    = $this->db->insert_id();
		$data2['mail_id']=$id_table1;
		$sender_id=$data2['sender_id'];
		$status="ok";
		$is_delete=0;
		$cteam = count($data2['receiver_id']);
		for($x = 0; $x < $cteam; $x++)
		{
			$sql = 'INSERT INTO `Z_mailsent` 
			(mail_id, sender_id, 
			receiver_id, status, is_delete)
			VALUES ("' . $id_table1 . '", "' . $sender_id . '", "'. $data2['receiver_id'][$x] . '", "' . $status . '", "'. $is_delete . '")';
			$query = $this->db->query($sql);
		}
		if ($query)
		{
			return $this->db->insert_id();
		}
		else
		{
			return "0";
		}
} 
public function deletemaildata($data)
{
	
	$this->db->where('message_id', $data['id']);
	$this->db->set('is_delete',true);
	$this->db->update('Z_Message');
	//DELETE FROM `table` WHERE id IN (264, 265)
}
public function getAlltrashdDataWhere($table,$select,$where,$key="")
	{
		
		
		$this->db->select($select);
		$this->db->from($table);
		$this->db->where($where);
		$this->db->where('is_delete',1);
		
		$query = $this->db->get();
		return ($query->num_rows() > 0) ? $query->result_array() : 0;
	}
	public function getsqldata()
	{
		 $this->db->select_max('message_id');
    $this->db->where('is_delete', 1);
    $query = $this->db->get('Z_Message');
    return $query->result_array_assoc();
	}
	public function getAllSentDataWhere($table,$select,$where,$key="")
	{
		
		
		$this->db->select($select);
		$this->db->from($table);
		$this->db->where($where);
		
		$query = $this->db->get();
		return ($query->num_rows() > 0) ? $query->result_array() : 0;
	}


}