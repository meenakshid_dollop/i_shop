
<?php
/*
Used for verfiy the email 
*/
function verifyEmail($email)
{
  return (!preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $email)) ? FALSE : TRUE;
}
/*
Used for verfiy the token 
*/
function verifiedToken($headers)
{
  $CI = &get_instance();
  $CI->load->library('Authorization_Token');
  $decodedToken = $CI->authorization_token->validateToken($headers['Authorization']);
  if (isset($decodedToken['data'])) {
    $var = (array)$decodedToken['data'];
  } else {
    $var = MESSAGE_conf::ERROR;
  }
  return $var;
}
//For Password Validation
function passwordValidation($password)
{
  $uppercase = preg_match('@[A-Z]@', $password);
  $lowercase = preg_match('@[a-z]@', $password);
  $number    = preg_match('@[0-9]@', $password);
  if ($uppercase && $lowercase && $number && strlen($password) > 6) {
    return 1;
  } else {
    0;
  }
}
//For mobile Validation
function mobileValidation($mobile)
{
  $number    = preg_match('/^[0-9]{10}+$/', $mobile);
  if ($number == 1) {
    return 1;
  } else {
    0;
  }
}
//For token Validation
function tokenVerification($headers)
{
  $CI = &get_instance(); 

  if (isset($headers['Authorization']) && ($headers['Authorization']) != null && !empty($headers['Authorization'])) {
    $result = verifiedToken($headers);
  
   
    if ($result == MESSAGE_conf::ERROR) {
      return $result = $CI->response(array("message" => MESSAGE_conf::INVALID_TOKEN), REST_Controller::HTTP_UNAUTHORIZED);
    }
    if($result['role']=="Admin")
    {
      return  array("id" => $result['id'], "role" => $result['role']);
    }
    else {
      return  array("id" => $result['id'], "mobile" => $result['mobile'], "role" => $result['role']);
    }
    
  } else {
    return $result = $CI->response(array("message" => MESSAGE_conf::TOKEN_REQUIRED), REST_Controller::HTTP_BAD_REQUEST);
  }
}




/////////////////////////// calculate time in days time hours
function time_Ago($time)
{
  
  // Calculate difference between current 
  // time and given timestamp in seconds 
  $time = strtotime($time);

  //$time = strtotime($time)-time();
  
  $diff     = time() - $time;
 
  // Time difference in seconds 
  $sec     = $diff;

  // Convert time difference in minutes 
  $min     = round($diff / 60);

  // Convert time difference in hours 
  $hrs     = round($diff / 3600);

  // Convert time difference in days 
  $days     = round($diff / 86400);

  // Convert time difference in weeks 
  $weeks     = round($diff / 604800);

  // Convert time difference in months 
  $mnths     = round($diff / 2600640);

  // Convert time difference in years 
  $yrs     = round($diff / 31207680);

  // Check for seconds 
  if ($sec <= 60) {
    return "$sec seconds ago";
  }

  // Check for minutes 
  else if ($min <= 60) {
    if ($min == 1) {
      return "one minute ago";
    } else {
      return "$min minutes ago";
    }
  }

  // Check for hours 
  else if ($hrs <= 24) {
    if ($hrs == 1) {
      return "an hour ago";
    } else {
      return "$hrs hours ago";
    }
  }

  // Check for days 
  else if ($days <= 7) {
    if ($days == 1) {
      return "Yesterday";
    } else {
      return "$days days ago";
    }
  }

  // Check for weeks 
  else if ($weeks <= 4.3) {
    if ($weeks == 1) {
      return "a week ago";
    } else {
      return "$weeks weeks ago";
    }
  }

  // Check for months 
  else if ($mnths <= 12) {
    if ($mnths == 1) {
      return "a month ago";
    } else {
      return "$mnths months ago";
    }
  }

  // Check for years 
  else {
    if ($yrs == 1
    ) {
      return "one year ago";
    } else {
      return "$yrs years ago";
    }
  }
}
/////////////////////////distance calculator
function distanceMeasure($lat2, $long2, $lat, $long,$userId) {
  $result = array();
  if (is_numeric($lat2) && is_numeric($long2)) {
    $pi80 = M_PI / 180;
    $lat *= $pi80;
    $long *= $pi80;
    $lat2 *= $pi80;
    $long2 *= $pi80;
    $r = 6372.797; // mean radius of Earth in km 
    $dlat = $lat2 - $lat;
    $dlon = $long2 - $long;
    $a = sin($dlat / 2) * sin($dlat / 2) + cos($lat) * cos($lat2) * sin($dlon / 2) * sin($dlon / 2);
    $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
    $result['km'] = $r * $c;
    $result['id'] = $userId;
    return $result;
  }

  function getRandomTokenForUserReferal()
  {   
    return generateRandomString();
  }
  function generateRandomString($length = 8)
  {
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }
}
?>