<?php //print_r($_SESSION['ConneKton_session']['admin_token']);die; 
?><?php include('includes/header.php') ?>;
<?php include('includes/sidebar.php') ?>;
<div class="main-content">
    <section class="section">
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>coupon List</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="Showcoupon" class="table table-striped table-hover" id="tableExport" style="width:100%;">
                                    <thead>
                                        <tr>
                                            <th>Coupon Image</th>
                                            <th>Coupon Title</th>
                                            <th>Max Amount</th>
                                            <th>Min Amount</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Active</th>
                                            <th>Delete</th>
                                            <th>Edit</th>
                                            <th>VIew</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="card card-static-2 mb-30">
                            <div class="card-body-table" id="showDetail">
                                <!-- <div class="shopowner-content-left pd-20 modalId">
                                    <div class="shopowner-dt-left">
                                        <h4>Vivian Harrell</h4>
                                    </div>
                                    <div class="shopowner-dts">
                                        <div class="shopowner-dt-list">
                                            <span class="left-dt"><b>Name</b></span>
                                            <span class="right-dt">Vivian Harrell</span>
                                        </div>
                                        <div class="shopowner-dt-list">
                                            <span class="left-dt"><b>Mobile Number</b></span>
                                            <span class="right-dt">9876543210</span>
                                        </div>
                                        <div class="shopowner-dt-list">
                                            <span class="left-dt"><b>Email</b></span>
                                            <span class="right-dt">sanFrancisco@gmail.com</span>
                                        </div>
                                        <div class="shopowner-dt-list">
                                            <span class="left-dt"><b>Plan Type</b></span>
                                            <span class="right-dt">Weekly</span>
                                        </div>
                                        <div class="shopowner-dt-list">
                                            <span class="left-dt"><b>Status</b></span>
                                            <span class="right-dt">Active</span>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('includes/footer.php') ?>;

<script>
    var token = '<?php echo $_SESSION['iWorkToken'] ?>';
    $(document).ready(function() {
        $.ajax({
            url: "<?php echo base_url('getCoupon') ?>",
            type: "GET",
            dataType: "json",
            headers: {
                'Authorization': token
            },
            success: function(data) {
                // console.log(data);
                var coupon = data.data;
                $.each(coupon, function(i, value) {
                    console.log(value);
                    if (coupon[i]['is_active'] == "1") {
                        id = coupon[i]['coupon_id'];
                        statusChange = 0;
                        var checked = "checked";
                    } else {
                        id = coupon[i]['coupon_id'];
                        var checked = "";
                        statusChange = 1;
                    }
                    toggle = "<div class='toggle-switch'><label class='toggle-switch switch'  id='swal-6'><input   type='checkbox' " + checked + " onclick='ChangeStatus(" + id + "," + statusChange + ")'   class= 'check " + id + "'   name = 'check' id = 'check'><span  class='slider-switch round'></span> </label></div>";
                    $("#Showcoupon tbody:last-child").append(
                        '<tr>' +
                        '<td><img width="50px" height="50px" src="' + coupon[i]['coupon_image'] + '"></td>' +
                        '<td>' + coupon[i]['coupon_title'] + '</td>' +
                        '<td>' + coupon[i]['max_amount'] + '</td>' +
                        '<td>' + coupon[i]['min_amount'] + '</td>' +
                        '<td>' + coupon[i]['start_date'] + '</td>' +
                        '<td>' + coupon[i]['end_date'] + '</td>' +
                        "<td>" + toggle + " </td> " +
                        '<td> <a href="#" class="deleteClass" onclick="deleteFunction(' + coupon[i]['coupon_id'] + ')"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#96a2b4" width="24px" height="24px"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM8 9h8v10H8V9zm7.5-5l-1-1h-5l-1 1H5v2h14V4z"></path></svg></a>  </td>' +
                        '<td> <a href="<?php echo base_url('add_coupon') ?>?id=' + coupon[i]['coupon_id'] + '"><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"/></svg></a></td>' +
                        '<td> <a href="javascript:void(0)" onclick="MyFunction(' + coupon[i]['coupon_id'] + ')" data-toggle="modal" data-target="#exampleModalCenter"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg></a></td>' +
                        '</tr>'
                    );
                });
            },
            error: function(data) {

            }
        });

    });


    function deleteFunction(id) {
        $.ajax({
            url: '<?php echo base_url() ?>addCoupon',
            type: 'POST',
            headers: {
                'Authorization': token
            },
            data: {
                is_delete: "deleted",
                coupon_id: id
            },
            dataType: 'json',
            error: function(response) {
                iziToast.error({
                    title: 'coupon',
                    message: "coupon Not Deleted ",
                    position: 'topRight'
                });
            },
            success: function(response) {
                if (response) {
                    iziToast.success({
                        title: 'coupon',
                        message: " coupon Deleted Succefully",
                        position: 'topRight'
                    });
                    setTimeout(function() {
                        window.location.reload(1);
                    }, 3000);
                }
            }

        });
    }

    function ChangeStatus(id, status) {
        // alert(id);
        $.ajax({
            url: '<?php echo base_url() ?>addCoupon',
            type: 'POST',
            headers: {
                'Authorization': token
            },
            data: {
                is_active: status,
                coupon_id: id
            },
            dataType: 'json',
            error: function(response) {
                iziToast.error({
                    title: 'coupon',
                    message: "coupon Status Not Change ",
                    position: 'topRight'
                });
            },
            success: function(response) {
                if (response) {
                    iziToast.success({
                        title: 'coupon',
                        message: " coupon Status Change Succefully",
                        position: 'topRight'
                    });
                    // setTimeout(function() {
                    //     window.location.reload(1);
                    // }, 3000);
                }
            }

        });
    }
</script>
<script>
    function MyFunction(id) {
        var file = "";
        var showDetails = "";
        var token = '<?php echo $_SESSION['iWorkToken'] ?>';
        $.ajax({
            url: '<?php echo base_url('getCoupon') ?>/' + id,
            type: 'GET',
            dataType: 'json',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': token
            },
            error: function() {
                // swal("Some Error", "error");
            },
            success: function(response) {
                var file1 = "";
                console.log(response);
                $.each(response.data, function(i, value) {
                    if (response.data['is_active'] == "1") {
                        active = "Active";
                    } else {
                        active = "Deactive";
                    }
                    if (response.data['user_type'] == "single_use") {
                        response.data['user_type'] = "Single Time";
                    } else {
                        response.data['user_type'] = "Multi Time";
                    }
                    showDetails = "<div class='shopowner-content-left pd-20 '>" +
                        "<div class='shopowner-dt-left'>" +
                        "<h4>Coupon</h4>" +
                        "</div>" +
                        "<div class='shopowner-dts'>" +
                        "<div class='shopowner-dt-list'>" +
                        "<span class='left-dt'><b>Title</b></span>" +
                        "<span class='right-dt'>" + response.data['coupon_title'] + "</span>" +
                        "</div>" +
                        "<div class='shopowner-dt-list'>" +
                        "<span class='left-dt'><b>Coupon Code</b></span>" +
                        "<span class='right-dt'>" + response.data['coupon_code'] + "</span>" +
                        "</div>" +
                        "<div class='shopowner-dt-list'>" +
                        "<span class='left-dt'><b>Discount(%)</b></span>" +
                        "<span class='right-dt'>" + response.data['discount_percent'] + "</span>" +
                        "</div>" +
                        "<div class='shopowner-dt-list'>" +
                        "<span class='left-dt'><b>Discount(₹)</b></span>" +
                        "<span class='right-dt'>" + response.data['discount_amount'] + "</span>" +
                        "</div>" +
                        "<div class='shopowner-dt-list'>" +
                        "<span class='left-dt'><b>Start Date+</b></span>" +
                        "<span class='right-dt'>" + response.data['start_date'] + "</span>" +
                        "</div>" +
                        "<div class='shopowner-dt-list'>" +
                        "<span class='left-dt'><b>End Date</b></span>" +
                        "<span class='right-dt'>" + response.data['end_date'] + "</span>" +
                        "</div>" +
                        "<div class='shopowner-dt-list'>" +
                        "<span class='left-dt'><b>Maximum Amount</b></span>" +
                        "<span class='right-dt'>" + response.data['max_amount'] + "</span>" +
                        "</div>" +
                        "<div class='shopowner-dt-list'>" +
                        "<span class='left-dt'><b>Minimum Amount</b></span>" +
                        "<span class='right-dt'>" + response.data['min_amount'] + "</span>" +
                        "</div>" +
                        "<div class='shopowner-dt-list'>" +
                        "<span class='left-dt'><b>Use Type</b></span>" +
                        "<span class='right-dt'>" + response.data['user_type'] + "</span>" +
                        "</div>" +
                        "<div class='shopowner-dt-list'>" +
                        "<span class='left-dt'><b>Status</b></span>" +
                        "<span class='right-dt'>" + active + "</span>" +
                        "</div>" +
                        "<div class='shopowner-dt-list'>" +
                        "<span class='left-dt'><b>Description</b></span>" +
                        "<span class='right-dt'>" + response.data['coupon_descrption'] + "</span>" +
                        "</div>" +
                        "</div>" +
                        "</div>";
                    $('#showDetail').html(showDetails);
                });
                $(".myimage3").on('error', function() {
                    $(this).prop('src', 'upload_images/post/pics_1.png');
                });
            }
        });
    }
</script>