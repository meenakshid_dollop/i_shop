<?php include('includes/header.php'); ?>
<?php include('includes/sidebar.php'); ?>
<!-- Main Content -->

<style type="text/css">
    .accordion-item-body {
        max-height: 100% !important;
    }
</style>

<div class="main-content">
    <section class="section">
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>All FAQ's</h4>
                            <a href="<?php echo base_url(); ?>faq" class="btn btn-primary" style="position: absolute;right: 20px;">Add New</a>
                        </div>
                        <div class="accordion">
                            <!-- <div class="accordion-item">
                  <div class="accordion-item-header" data-role='collapsible-set' data-content-theme='d' id='set'>
                    What is Web Development?
                  </div>
                  <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                      Web Development broadly refers to the tasks associated with developing functional websites and applications for the Internet. The web development process includes web design, web content development, client-side/server-side scripting and network security configuration, among other tasks.
                    </div>
                  </div>
                </div>
                <div class="accordion-item">
                  <div class="accordion-item-header">
                    What is Web Development?
                  </div>
                  <div class="accordion-item-body">
                    <div class="accordion-item-body-content">
                      Web Development broadly refers to the tasks associated with developing functional websites and applications for the Internet. The web development process includes web design, web content development, client-side/server-side scripting and network security configuration, among other tasks.
                    </div>
                  </div>
                </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php include('includes/footer.php'); ?>

<script type="text/javascript">
    // const accordionItemHeaders = document.querySelectorAll(".accordion-item-header");

    // accordionItemHeaders.forEach(accordionItemHeader => {
    //   accordionItemHeader.addEventListener("click", event => {
    //     accordionItemHeader.classList.toggle("active");
    //     const accordionItemBody = accordionItemHeader.nextElementSibling;
    //     console.log(accordionItemBody);
    //     if (accordionItemHeader.classList.contains("active")) {
    //       accordionItemBody.style.maxHeight = accordionItemBody.scrollHeight + "px";
    //     } else {
    //       accordionItemBody.style.maxHeight = 0;
    //     }

    //   });
    // });
    // $(".accordion-item-header").click(function() {
    //   $(".accordion-item-body").toggle();
    // });
</script>

<script type="text/javascript">
    var token = '<?php echo $_SESSION['iWorkToken'] ?>';
    $(document).ready(function() {
        $(document).on('click', '.accordion-item-header', function() {
            $(this).next().toggleClass("collapse");
        });
        var showcontent = "";
        var showabout = "";


        $.ajax({
            url: '<?php echo base_url() ?>getFaq',
            type: 'GET',
            dataType: 'json',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': token
            },
            error: function() {
                swal("Some Error", "error");
            },
            success: function(response) {
                // console.log(response);
                var title = new Array();
                var content = new Array();
                var titleArray = new Array();
                var idsArray = new Array();
                var contentArray = new Array();

                $.each(response.faq, function(i, value) {
                    var val = JSON.parse(value.content);

                    title = JSON.parse(val.title);
                    content = JSON.parse(val.content);
                    titleArray.push(title);
                    idsArray.push(value.id);
                    contentArray.push(content);
                });
                //console.log(idsArray);
                var titleArrayData = [].concat.apply([], titleArray);
                var contentArrayData = [].concat.apply([], contentArray);
                for (let l = 0; l < titleArrayData.length; l++) {
                    showcontent += '<div class="accordion-item">' +
                        '<div class="accordion-item-header " id = "accordion-item-header">' +
                        titleArrayData[l] +
                        '</div>' +
                        "<div id='accordion-item-body collapse' class='accordion-item-body collapse  in'>" +
                        "<div  class='accordion-item-body-content' >" +
                        contentArrayData[l] +
                        "</div>" +
                        "</div>" +
                        "<a href='<?php echo base_url('faq'); ?>?id=" + idsArray[l] + "' class='editClass'><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-edit'><path d='M11 4H4a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2v-7'></path><path d='M18.5 2.5a2.121 2.121 0 0 1 3 3L12 15l-4 1 1-4 9.5-9.5z'></path></svg></a>" +
                        // "<a href='<?php echo base_url('faq'); ?>?id="+idsArray[l]+"' class='editClass'><i data-feather='settings'></i></a>"+
                        "<a href='#' class='deleteClass' onclick = 'deleteFunction(" + idsArray[l] + ")'><svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24' fill='#96a2b4' width='24px' height='24px'><path d='M0 0h24v24H0V0z' fill='none'/><path d='M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM8 9h8v10H8V9zm7.5-5l-1-1h-5l-1 1H5v2h14V4z'/></svg></a>" +

                        "</div>";
                    $('.accordion').html(showcontent);
                }
            }
        });

    });
</script>
<script>
    function deleteFunction(id) {
        //var Token = '<?php echo $this->session->userdata("token"); ?>';

       // alert(id);
        $.ajax({
            url: '<?php echo base_url() ?>deleteFaq/' + id,
            type: 'DELETE',
            dataType: 'json',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': token
            },
            error: function() {

            },
            success: function(response) {
                iziToast.success({
                    title: 'Faq',
                    message: " Faq Deleted Succefully",
                    position: 'topRight'
                });
                     location.reload();
            }
        });

    }
</script>

<style>
    .accordion-item {
        position: relative;
        width: 95%;
        background-color: #2928ba !important;
    }

    .deleteClass {
        position: absolute;
        right: -80px;
        top: 15px;
    }

    .editClass {
        position: absolute;
        right: -40px;
        top: 15px;
    }
</style>