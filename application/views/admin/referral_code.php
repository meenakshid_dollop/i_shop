<?php include('includes/header.php'); ?>
<?php include('includes/sidebar.php'); ?>
<div class="main-content">
    <section class="section">
        <div class="section-body">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-6">
                    <div class="card">
                        <form class="needs-validation" name="referralForm" id="referralForm" novalidate="">
                            <div class="card-header">
                                <h4>Add Referral Code</h4>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Referral Code</label>
                                    <input type="hidden" id="id" name="id" value="<?php if (isset($getReferralCodeById['referral_id']) && $getReferralCodeById['referral_id'] != "") {
                                                                                        echo $getReferralCodeById['referral_id'];
                                                                                    } ?>">
                                    <input type="text" value="<?php if (isset($getReferralCodeById['ref_code']) && $getReferralCodeById['ref_code'] != "") {
                                                                    echo $getReferralCodeById['ref_code'];
                                                                } ?>" class="form-control" id="ref_code" name="ref_code" required="">

                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button type="submit" class="btn btn-primary" id="submitBtn">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
</div>
<?php include('includes/footer.php'); ?>
<script type="text/javascript">
    var token = '<?php echo $_SESSION['iWorkToken'] ?>';


    $(document).ready(function() {
        $("#categoryForm").validate({
            rules: {
                //catImage: 'required',
                ref_code: 'required',
            },
            message: {
                ref_code: "Please Enter The Category TItle",

            }
        });

        $("#submitBtn").click(function(e) {

            e.preventDefault();
            var fd = new FormData();
           // console.log(files);
            var ref_code = $('#ref_code').val();
            var ref_code_id = $('#id').val();
           // alert(referral_id);
           // console.log(categoryName);
            fd.append('ref_code', ref_code);
            fd.append('ref_code_id', ref_code_id);
            $.ajax({
                url: '<?php echo base_url() ?>referralCode',
                type: 'POST',
                data: fd,
                cache: false,
                contentType: false,
                processData: false,
                headers: {
                    'Authorization': token
                },

                error: function(xhr) {
                    console.log(xhr);
                    iziToast.error({
                        title: 'Referral Code',
                        message: xhr.responseJSON.message,
                        position: 'topRight'
                    });
                },
                success: function(response) {
                    <?php if (isset($getReferralCodeById['referral_id']) && $getReferralCodeById['referral_id'] != "") {
                    ?>
                        iziToast.success({
                            title: 'Referral Code',
                            message: 'Referral Code Update Successfully',
                            position: 'topRight'
                        });
                    <?php } else { ?>
                        iziToast.success({
                            title: 'Referral Code',
                            message: 'Referral Code Added Successfully',
                            position: 'topRight'
                        });
                    <?php  } ?>

                    window.location = "<?php echo base_url("view_referral_code"); ?>";
                }
            });
        });

    });
</script>