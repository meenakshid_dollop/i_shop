<?php include('includes/header.php'); ?>
<?php include('includes/sidebar.php'); ?>
<div class="main-content">
    <section class="section">
        <div class="section-body">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-6">
                    <div class="card">
                        <form class="needs-validation" name="passwordForm" id="passwordForm" novalidate="">
                            <div class="card-header">
                                <h4>Change Password</h4>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Old Password</label>
                                    <input type="password" value="" class="form-control" id="old_password" name="old_password" required="">

                                </div>
                                <div class="form-group">
                                    <label>New Password</label>
                                    <input type="password" value="" class="form-control" id="new_password" name="new_password" required="">

                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button type="submit" class="btn btn-primary" id="submitBtn">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
</div>
<?php include('includes/footer.php'); ?>


<script type="text/javascript">
    var token = '<?php echo $_SESSION['iWorkToken'] ?>';
    $(document).ready(function() {

        $("#submitBtn").click(function(e) {
            // alert("lk;lk");
            e.preventDefault();
            var old_password = $('#old_password').val();
            var new_password = $('#new_password').val();
            // alert(Content);
            $.ajax({
                url: '<?php echo base_url(); ?>change_admin_password',
                type: 'PUT',
                dataType: 'json',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': token
                },
                data: {
                    old_password: old_password,
                    new_password: new_password
                },
                error: function(xhr) {
                    // console.log(xhr);
                    iziToast.error({
                        title: 'Password',
                        message: xhr.responseJSON.message,
                        position: 'topRight'
                    });
                },
                success: function(response) {
                   // alert("kj");
                    iziToast.success({
                        title: 'Password',
                        message: " Password Change Succefully",
                        position: 'topRight'
                    });

                }
            });
        });
    });
</script>