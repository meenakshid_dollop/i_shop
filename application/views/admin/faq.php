<?php include('includes/header.php'); ?>
<?php include('includes/sidebar.php'); ?>
<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <form method="post" id="Faqform">
                            <div class="card-header">
                                <h4>FAQ's</h4>
                            </div>
                            <div class="card-body">
                                <div id="items">
                                    <div class="form-group row mb-4">

                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Title</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="title[]" class="form-control titleClass">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Content</label>
                                        <div class="col-sm-12 col-md-7">
                                            <textarea class="form-control contentClass" name="content[]"></textarea>
                                        </div>
                                        <div class="col-sm-2 col-md-2">
                                            <div class="">
                                                <a id="add" href="#"><i class="fas fa-plus-circle"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                    <div class="col-sm-12 col-md-7">
                                        <button type="submit" id="addfaq" onclick="return false;" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </section>
</div>
<?php include('includes/footer.php'); ?>


<script type="text/javascript">
    $(document).ready(function() {
        $(".delete").hide();
        //when the Add Field button is clicked
        $("#add").click(function(e) {
            $(".delete").fadeIn("1500");
            //Append a new row of code to the "#items" div
            $("#items").append(
                '<div class="card-body1"><div class="form-group row mb-4"><label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Title</label><div class="col-sm-12 col-md-7"><input type="text" name="title[]" class="form-control"></div></div><div class="form-group row mb-4"><label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Content</label><div class="col-sm-12 col-md-7"><textarea name="content[]" class="form-control"></textarea></div><div class="col-sm-2 col-md-2"><div class=""><a class="delete" href="#"><i class="fas fa-minus-circle"></i></a></div></div></div></div>'
            );
        });
        $("body").on("click", ".delete", function(e) {
            $(".card-body1").last().remove();
        });
    });
</script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#Faqform").validate({
            rules: {
                title: 'required',
                content: 'required'
            }
        });
    });
</script>
<script src="assets/bundles/izitoast/js/iziToast.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        // var title = array();
        var token = '<?php echo $_SESSION['iWorkToken'] ?>';
        // if (Token == "") {
        //     window.location.href = "<?php echo base_url(); ?>login";
        // } else {
        <?php if (isset($_GET['id']) && $_GET['id'] != "") { ?>
            $.ajax({
                type: "GET",
                url: "<?php echo base_url() ?>getFaq/<?php echo $_GET['id'] ?>",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': token
                },
                dataType: 'json',
                success: function(res) {
                    console.log(res);
                    var val = JSON.parse(res.faq.content);
                    // console.log(val);
                    title = JSON.parse(val.title);
                    content = JSON.parse(val.content);
                    console.log(title);
                    console.log(content);
                    $('.contentClass').text(content);
                    $('.titleClass').val(title);
                    $('#add').hide();

                }

            });
        <?php } ?>
        $("#addfaq").click(function() {
            var faq_id = "";
            faq_id = '<?php if (isset($_GET['id']) && $_GET['id'] != "") {
                            echo $_GET['id'];
                        } ?>'
            var data = ""
            var title = $("input[name='title[]']")
                .map(function() {
                    return $(this).val();
                }).get();
            if (title == "") {
                title = null;
            }
            if (content == "") {
                content = null;
            }
            var content = $("textarea[name='content[]']")
                .map(function() {
                    return $(this).val();
                }).get();

            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>addFAQByAdmin",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': token
                },
                dataType: 'html',
                data: {
                    title: title,
                    content: content,
                    faq_id: faq_id
                },
                error: function() {
                    iziToast.error({
                        title: 'Not Created',
                        message: 'Please Enter Content',
                        position: 'topRight'
                    });
                },
                success: function(res) {

                    if (res) {
                        <?php if (isset($_GET['id']) && $_GET['id'] != "") { ?>
                            swal("Faq Updated", "", "success");
                        <?php } else { ?>
                            swal("Faq Created", "", "success");
                        <?php

                        } ?>
                    }
                    window.location = "<?php echo base_url(); ?>view_faq";

                }

            });
        });
        $("#addfaq").submit(function() {
            e.preventDefault();

        });
        //}
    });
</script>