<?php include('includes/header.php') ?>
<?php include('includes/sidebar.php') ?>
<div class="main-content">
	<section class="section">
		<div class="row">
			<div class="col-xl-3 col-lg-6">
				<div class="card">
					<div class="card-bg">
						<div class="p-t-20 d-flex justify-content-between">
							<div class="col">
								<h6 class="mb-0">Total User</h6>
								<span class="font-weight-bold mb-0 font-20">1,562</span>
							</div>
							<i class="fas fa-diagnoses card-icon col-orange font-30 p-r-30"></i>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-3 col-lg-6">
				<div class="card">
					<div class="card-bg">
						<div class="p-t-20 d-flex justify-content-between">
							<div class="col">
								<h6 class="mb-0">Total Employers</h6>
								<span class="font-weight-bold mb-0 font-20">895</span>
							</div>
							<i class="fas fa-diagnoses card-icon col-green font-30 p-r-30"></i>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-3 col-lg-6">
				<div class="card">
					<div class="card-bg">
						<div class="p-t-20 d-flex justify-content-between">
							<div class="col">
								<h6 class="mb-0">Total KP'S</h6>
								<span class="font-weight-bold mb-0 font-20">895</span>
							</div>
							<i class="fas fa-diagnoses card-icon col-green font-30 p-r-30"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<?php include('includes/footer.php') ?>