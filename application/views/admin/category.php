<?php include('includes/header.php'); ?>
<?php include('includes/sidebar.php'); ?>
<div class="main-content">
    <section class="section">
        <div class="section-body">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-6">
                    <div class="card">
                        <form class="needs-validation" name="categoryForm" id="category" novalidate="">
                            <div class="card-header">
                                <h4>Add Category</h4>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Category Name</label>
                                    <input type="hidden" id="id" name="id" value="<?php if (isset($getCategoryById['category_id']) && $getCategoryById['category_id'] != "") {
                                                                                       echo $getCategoryById['category_id'];
                                                                                    } ?>">
                                    <input type="text" value="<?php if (isset($getCategoryById['category_name']) && $getCategoryById['category_name'] != "") {
                                                                    echo $getCategoryById['category_name'];
                                                                } ?>" class="form-control" id="category_name" name="categoryName" required="">

                                </div>
                                <div class="form-group">
                                    <label>Category Image</label>
                                    <input type="file" name="category_image" id="file" class="form-control" required="">
                                    <img src="<?php if (isset($getCategoryById['category_image']) && $getCategoryById['category_image'] != "") {
                                                    echo $getCategoryById['category_image'];
                                                } else { ?>assets/default.png<?php } ?>" id="pereviewImage" width="100px" height="100px">
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button type="submit" class="btn btn-primary" id="submitBtn">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
</div>
<?php include('includes/footer.php'); ?>
<script type="text/javascript">
    var token = '<?php echo $_SESSION['iWorkToken'] ?>';

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#pereviewImage').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
    }

    $("#file").change(function() {
        readURL(this);
    });

    $(document).ready(function() {
        $("#categoryForm").validate({
            rules: {               
                category_name: 'required',
            },
            message: {
                category_name: "Please Enter The Category TItle",

            }
        });

        $("#submitBtn").click(function(e) {

            e.preventDefault();
            var fd = new FormData();
            var files = $("#file").prop("files")[0];
            console.log(files);
            var categoryName = $('#category_name').val();
            var category_id = $('#id').val();          
            console.log(categoryName);
            fd.append('categoryName', categoryName);
            fd.append('category_image', files);
            fd.append('category_id', category_id);
            $.ajax({
                url: '<?php echo base_url() ?>categories',
                type: 'POST',
                data: fd,
                cache: false,
                contentType: false,
                processData: false,
                headers: {
                    'Authorization': token
                },
                error: function(xhr) {                 
                    iziToast.error({
                        title: 'Category',
                        message: xhr.responseJSON.message,
                        position: 'topRight'
                    });
                },
                success: function(response) {
                    <?php if (isset($getCategoryById['category_id']) && $getCategoryById['category_id'] != "") {
                    ?>
                        iziToast.success({
                            title: 'Category',
                            message: 'Category Update Successfully',
                            position: 'topRight'
                        });
                    <?php } else { ?>
                        iziToast.success({
                            title: 'Category',
                            message: 'Category Added Successfully',
                            position: 'topRight'
                        });
                    <?php  } ?>

                    window.location = "<?php echo base_url("view_category"); ?>";
                }
            });
        });

    });
</script>