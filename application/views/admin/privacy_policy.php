<?php include('includes/header.php'); ?>
<?php include('includes/sidebar.php'); ?>
<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">

                        <div class="card-header">
                            <h4>Privacy & Policy</h4>

                        </div>
                        <div class="card-body">
                            <form method="post">
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Content</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="hidden" name="id" id="policy_id" value="<?php if (isset($policy['id']) && $policy['id'] != "") {
                                                                                                    echo $policy['id'];
                                                                                                } ?>">
                                        <textarea class="summernote" value="<?php if (isset($policy['policy']) && $policy['policy'] != "") {
                                                                                echo $policy['policy'];
                                                                            } ?>" id="policy"><?php if (isset($policy['policy']) && $policy['policy'] != "") {
                                                                                                    echo $policy['policy'];
                                                                                                } ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                    <div class="col-sm-12 col-md-7">
                                        <button class="btn btn-primary" type="submit" id="SubmitBtn">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php include('includes/footer.php'); ?>
<script type="text/javascript">
    var token = '<?php echo $_SESSION['iWorkToken'] ?>';
    $(document).ready(function() {
        var showTerm = "";
        var policy = "";
        $("#SubmitBtn").click(function(e) {
           // e.preventDefault();
            var policyId = $('#policy_id').val();
           // alert(policyId);
            var Content = $('.summernote').summernote('code');
            //alert(Content);
            $.ajax({
                url: '<?php echo base_url(); ?>updatePolicy',
                type: 'POST',
                dataType: 'json',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': token
                },
                data: {
                    policy: Content,
                    policy_id: policyId
                },
                error: function(xhr, status, error) {
                    var errorMessage = xhr.status + ': ' + xhr.statusText
                    // alert('Error - ' + errorMessage);
                },
                success: function(response) {
                 window.location = "<?php echo base_url(); ?>view_privacy";

                }
            });
        });
    });
</script>