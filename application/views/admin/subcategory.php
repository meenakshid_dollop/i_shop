<?php include('includes/header.php');
?>
<?php include('includes/sidebar.php'); ?>
<div class="main-content">
    <section class="section">
        <div class="section-body">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-6">
                    <div class="card">
                        <form class="needs-validation" name="subCategoryForm" id="subcategory" novalidate="">
                            <div class="card-header">
                                <h4>Add Sub Category</h4>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Subcategory Name</label>
                                    <input type="hidden" id="id" name="id" value="<?php if (isset($getSubCategoryById['sub_category_id']) && $getSubCategoryById['sub_category_id'] != "") {
                                                                                        echo $getSubCategoryById['sub_category_id'];
                                                                                    } ?>">
                                    <input type="text" class="form-control" id="sub_category_name" name="sub_category_name" value="<?php if (isset($getSubCategoryById['sub_category_name']) && $getSubCategoryById['sub_category_name'] != "") {
                                                                                                                                        echo $getSubCategoryById['sub_category_name'];
                                                                                                                                    } ?>" required="">

                                </div>
                                <div class="form-group">
                                    <label>Category</label>
                                    <select name="category_id" onclick="Myfunction()" id="category_id" class="form-control">
                                        <option value="">Select Category</option>
                                        <?php if (isset($category) && $category != "") {
                                            foreach ($category as $categoryList) {
                                        ?>
                                                <option <?php if (isset($getSubCategoryById['category_id']) && $getSubCategoryById['category_id'] == $categoryList['category_id']) {
                                                            echo "selected";
                                                        } ?> value="<?php if ($categoryList["category_id"] != "") {
                                                                        echo $categoryList["category_id"];
                                                                    }  ?>"><?php if (isset($categoryList['category_name']) && $categoryList['category_name'] != "") {
                                                                                echo $categoryList['category_name'];
                                                                            } ?></option>
                                        <?php }
                                        } ?>


                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Subcategory Image</label>
                                    <input type="file" name="sub_category_image" id="file" class="form-control" required="">
                                    <img src="<?php if (isset($getSubCategoryById['sub_category_image']) && $getSubCategoryById['sub_category_image'] != "") {
                                                    echo $getSubCategoryById['sub_category_image'];
                                                } else { ?>assets/default.png<?php } ?>" id="pereviewImage" width="100px" height="100px">
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button type="submit" class="btn btn-primary" id="submitBtn">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
</div>
<?php include('includes/footer.php'); ?>
<script type="text/javascript">
    var token = '<?php echo $_SESSION['iWorkToken'] ?>';

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#pereviewImage').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
    }

    $("#file").change(function() {
        readURL(this);
    });

    $(document).ready(function() {
        $("#subCategoryForm").validate({
            rules: {
                //catImage: 'required',
                sub_category_name: 'required',
            },
            message: {
                sub_category_name: "Please Enter The Sub Category TItle",

            }
        });

        $("#submitBtn").click(function(e) {

            e.preventDefault();
            var fd = new FormData();
            var files = $("#file").prop("files")[0];
            console.log(files);
            var categoryName = $('#sub_category_name').val();
            var category_id = $('#category_id').val();
            var sub_category_id = $('#id').val();
            // alert(category_id);
            console.log(categoryName);
            fd.append('sub_category_name', categoryName);
            fd.append('category_id', category_id);
            fd.append('sub_category_id', sub_category_id);
            fd.append('sub_category_image', files);
            $.ajax({
                url: '<?php echo base_url() ?>subCategories',
                type: 'POST',
                data: fd,
                cache: false,
                contentType: false,
                processData: false,
                headers: {
                    'Authorization': token
                },

                error: function(xhr) {
                    console.log(xhr);
                    iziToast.error({
                        title: 'Subcategory',
                        message: xhr.responseJSON.message,
                        position: 'topRight'
                    });
                },
                success: function(response) {
                    <?php if (isset($getSubCategoryById['sub_category_id']) && $getSubCategoryById['sub_category_id'] != "") {
                    ?>
                        iziToast.success({
                            title: 'Subcategory',
                            message: 'Subcategory Update Successfully',
                            position: 'topRight'
                        });
                    <?php } else { ?>
                        iziToast.success({
                            title: 'Subcategory',
                            message: 'Subcategory Added Successfully',
                            position: 'topRight'
                        });
                    <?php  } ?>

                     window.location = "<?php echo base_url("view_subcategory"); ?>";
                }
            });
        });

    });
</script>