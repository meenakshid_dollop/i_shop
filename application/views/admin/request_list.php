<?php //print_r($_SESSION['ConneKton_session']['admin_token']);die; 
?><?php include('includes/header.php') ?>;
<?php include('includes/sidebar.php') ?>;
<div class="main-content">
    <section class="section">
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>coupon List</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="Showcoupon" class="table table-striped table-hover" id="tableExport" style="width:100%;">
                                    <thead>
                                        <tr>
                                            <th>Image</th>
                                            <th> Name</th>
                                            <th>Gender</th>
                                            <th>Referral Code</th>
                                            <th>Active</th>
                                            <th>Delete</th>
                                            <th>Admin Verified</th>
                                            <th>View</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="card card-static-2 mb-30">
                            <div class="card-body-table" id="showDetail">
                                <!-- <div class="shopowner-content-left pd-20 modalId">
                                    <div class="shopowner-dt-left">
                                        <h4>Vivian Harrell</h4>
                                    </div>
                                    <div class="shopowner-dts">
                                        <div class="shopowner-dt-list">
                                            <span class="left-dt"><b>Name</b></span>
                                            <span class="right-dt">Vivian Harrell</span>
                                        </div>
                                        <div class="shopowner-dt-list">
                                            <span class="left-dt"><b>Mobile Number</b></span>
                                            <span class="right-dt">9876543210</span>
                                        </div>
                                        <div class="shopowner-dt-list">
                                            <span class="left-dt"><b>Email</b></span>
                                            <span class="right-dt">sanFrancisco@gmail.com</span>
                                        </div>
                                        <div class="shopowner-dt-list">
                                            <span class="left-dt"><b>Plan Type</b></span>
                                            <span class="right-dt">Weekly</span>
                                        </div>
                                        <div class="shopowner-dt-list">
                                            <span class="left-dt"><b>Status</b></span>
                                            <span class="right-dt">Active</span>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('includes/footer.php') ?>;

<script>
    var token = '<?php echo $_SESSION['iWorkToken'] ?>';
    $(document).ready(function() {
        $.ajax({
            url: "<?php echo base_url('getUserByAdmin') ?>",
            type: "GET",
            dataType: "json",
            headers: {
                'Authorization': token
            },
            success: function(data) {
                console.log(data);
                var user = data.data;
                $.each(user, function(i, value) {
                    console.log(value);
                    if (user[i]['is_active'] == "1") {
                        id = user[i]['user_id'];
                        statusChange = 0;
                        var checked = "checked";
                    } else {
                        id = user[i]['user_id'];
                        var checked = "";
                        statusChange = 1;
                    }
                    if (user[i]['user_profile'] == "") {
                        user[i]['user_profile'] = "assets/default.png";
                    }
                    if (user[i]['admin_verfiy'] == 1) {
                        adminStatus = 0;
                        circleCheck = '<svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 15l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z"/></svg>';
                    } else {
                        adminStatus = 1;
                        circleCheck = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="24" height="24"><path d="M0 0h24v24H0V0zm0 0h24v24H0V0z" fill="none"/><path d="M16.59 7.58L10 14.17l-3.59-3.58L5 12l5 5 8-8zM12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8z"/></svg>';

                    }
                    toggle = "<div class='toggle-switch'><label class='toggle-switch switch'  id='swal-6'><input   type='checkbox' " + checked + " onclick='ChangeStatus(" + id + "," + statusChange + ")'   class= 'check " + id + "'   name = 'check' id = 'check'><span  class='slider-switch round'></span> </label></div>";
                    $("#Showcoupon tbody:last-child").append(
                        '<tr>' +
                        '<td><img width="50px" height="50px" src="' + user[i]['user_profile'] + '"></td>' +
                        '<td>' + user[i]['user_name'] + '</td>' +
                        '<td>' + user[i]['gender'] + '</td>' +
                        '<td>' + user[i]['referral_code'] + '</td>' +
                        "<td>" + toggle + " </td> " +
                        '<td> <a href="#" class="deleteClass" onclick="deleteFunction(' + user[i]['user_id'] + ')"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#96a2b4" width="24px" height="24px"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM8 9h8v10H8V9zm7.5-5l-1-1h-5l-1 1H5v2h14V4z"></path></svg></a>  </td>' +
                        '<td> <a href="#" class="deleteClass" onclick="adminVerify(' + user[i]['user_id'] + ',' + adminStatus + ')"> ' + circleCheck + '</a> </td>' +
                        '<td> <a href="javascript:void(0)" onclick="MyFunction(' + user[i]['user_id'] + ')" data-toggle="modal" data-target="#exampleModalCenter"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg></a></td>' +
                        '</tr>'
                    );
                });
            },
            error: function(data) {

            }
        });

    });


    function deleteFunction(id) {
        $.ajax({
            url: '<?php echo base_url() ?>updateUserByAdmin/' + id,
            type: 'POST',
            headers: {
                'Authorization': token
            },
            data: {
                is_delete: "deleted",
                // user_id: id
            },
            dataType: 'json',
            error: function(response) {
                iziToast.error({
                    title: 'coupon',
                    message: "coupon Not Deleted ",
                    position: 'topRight'
                });
            },
            success: function(response) {
                if (response) {
                    iziToast.success({
                        title: 'coupon',
                        message: " coupon Deleted Succefully",
                        position: 'topRight'
                    });
                    setTimeout(function() {
                        window.location.reload(1);
                    }, 3000);
                }
            }

        });
    }

    function ChangeStatus(id, status) {
        // alert(id);
        $.ajax({
            url: '<?php echo base_url() ?>updateUserByAdmin/' + id,
            type: 'PUT',
            headers: {
                'Authorization': token
            },
            data: {
                is_active: status,
                // user_id: id
            },
            dataType: 'json',
            error: function(response) {
                iziToast.error({
                    title: 'coupon',
                    message: "coupon Status Not Change ",
                    position: 'topRight'
                });
            },
            success: function(response) {
                if (response) {
                    iziToast.success({
                        title: 'coupon',
                        message: " coupon Status Change Succefully",
                        position: 'topRight'
                    });
                    setTimeout(function() {
                        window.location.reload(1);
                    }, 3000);
                }
            }

        });
    }

    function adminVerify(id, adminStatus) {
        alert(adminStatus);
        $.ajax({
            url: '<?php echo base_url() ?>updateUserByAdmin/' + id,
            type: 'PUT',
            headers: {
                'Authorization': token
            },
            data: {
                admin_verfiy: adminStatus,
                // user_id: id
            },
            dataType: 'json',
            error: function(response) {
                iziToast.error({
                    title: 'User',
                    message: "User Status Not Change ",
                    position: 'topRight'
                });
            },
            success: function(response) {
                if (response) {
                    iziToast.success({
                        title: 'User',
                        message: " User Status Change Succefully",
                        position: 'topRight'
                    });
                    setTimeout(function() {
                        window.location.reload(1);
                    }, 3000);
                }
            }

        });
    }
</script>
<script>
    function MyFunction(id) {
        var file = "";
        var showDetails = "";
        var token = '<?php echo $_SESSION['iWorkToken'] ?>';
        $.ajax({
            url: '<?php echo base_url('getUserByAdmin') ?>/' + id,
            type: 'GET',
            dataType: 'json',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': token
            },
            error: function() {
                // swal("Some Error", "error");
            },
            success: function(response) {
                var file1 = "";
                console.log(response);
                $.each(response.data, function(i, value) {
                    if (response.data['is_active'] == "1") {
                        active = "Active";
                    } else {
                        active = "Deactive";
                    }
                    if (response.data['admin_verfiy'] == "1") {
                        admin_verfiy = "Verfified";
                    } else {
                        admin_verfiy = "Unverified";
                    }
                    if (response.data['user_type'] == "single_use") {
                        response.data['user_type'] = "Single Time";
                    } else {
                        response.data['user_type'] = "Multi Time";
                    }
                    showDetails = "<div class='shopowner-content-left pd-20 '>" +
                        "<div class='shopowner-dt-left'>" +
                        "<h4>User</h4>" +
                        "</div>" +
                        "<div class='shopowner-dts'>" +
                        "<div class='shopowner-dt-list'>" +
                        "<span class='left-dt'><b>Name</b></span>" +
                        "<span class='right-dt'>" + response.data['user_name'] + "</span>" +
                        "</div>" +
                        "<div class='shopowner-dt-list'>" +
                        "<span class='left-dt'><b>Mobile</b></span>" +
                        "<span class='right-dt'>" + response.data['mobile'] + "</span>" +
                        "</div>" +
                        "<div class='shopowner-dt-list'>" +
                        "<span class='left-dt'><b>Gender</b></span>" +
                        "<span class='right-dt'>" + response.data['gender'] + "</span>" +
                        "</div>" +
                        "<div class='shopowner-dt-list'>" +
                        "<span class='left-dt'><b>Referral Code</b></span>" +
                        "<span class='right-dt'>" + response.data['referral_code'] + "</span>" +
                        "</div>" +
                        "<div class='shopowner-dt-list'>" +
                        "<span class='left-dt'><b>Address</b></span>" +
                        "<span class='right-dt'>" + response.data['user_address'] + "</span>" +
                        "</div>" +
                        "<div class='shopowner-dt-list'>" +
                        "<span class='left-dt'><b>Admin Verified</b></span>" +
                        "<span class='right-dt'>" + admin_verfiy + "</span>" +
                        "</div>" +
                        "<div class='shopowner-dt-list'>" +
                        "<span class='left-dt'><b>Status</b></span>" +
                        "<span class='right-dt'>" + active + "</span>" +
                        "</div>" +
                        "<div class='shopowner-dt-list'>" +
                        "<span class='left-dt'><b>Government Documnet</b></span>" +
                        "<span class='right-dt'><img width='200px' height='200px' src = '" + response.data['government_id'] + "'></span>" +
                        "</div>" +

                        "</div>" +
                        "</div>";
                    $('#showDetail').html(showDetails);
                });
                $(".myimage3").on('error', function() {
                    $(this).prop('src', 'upload_images/post/pics_1.png');
                });
            }
        });
    }
</script>