<div class="main-sidebar sidebar-style-2">
	<aside id="sidebar-wrapper">
		<div class="sidebar-brand">
			<a href="<?php echo base_url('dashboard'); ?>"> <img alt="image" src="<?php echo base_url(); ?>assets/logo.png" class="header-logo" />
			</a>
		</div>
		<ul class="sidebar-menu">
			<li class="dropdown active"><a href="<?php echo base_url('dashboard'); ?>" class="nav-link"><i data-feather="monitor"></i><span>Dashboard</span>
				</a>
			</li>
			<!-- <li class="dropdown"><a href="#" class="nav-link has-dropdown"><i data-feather="users"></i><span>User Managment</span></a>
				<ul class="dropdown-menu">
					<li><a class="nav-link" href="javascript:void(0)">Users</a></li>
					<li><a class="nav-link" href="javascript:void(0)">Employers</a></li>
					<li><a class="nav-link" href="javascript:void(0)">KP's</a></li>
				</ul>
			</li> -->
			<li class="dropdown"><a href="#" class="nav-link has-dropdown"><i data-feather="user"></i><span>User Managment</span></a>
				<ul class="dropdown-menu">
					<li><a class="nav-link" href="<?php echo base_url("view_user"); ?>">User List</a></li>


				</ul>
			</li>
			<li class="dropdown"><a href="#" class="nav-link has-dropdown"><i data-feather="send"></i><span>Category Managment</span></a>
				<ul class="dropdown-menu">
					<li><a class="nav-link" href="<?php echo base_url("category"); ?>">Add Category</a></li>
					<li><a class="nav-link" href="<?php echo base_url("view_category"); ?>">View Category</a></li>

				</ul>
			</li>
			<li class="dropdown"><a href="#" class="nav-link has-dropdown"><i data-feather="send"></i><span>Subcategory Managment</span></a>
				<ul class="dropdown-menu">
					<li><a class="nav-link" href="<?php echo base_url("subcategory"); ?>">Add Subcategory</a></li>
					<li><a class="nav-link" href="<?php echo base_url("view_subcategory"); ?>">View Subcategory</a></li>

				</ul>
			</li>

			<li class="dropdown"><a href="#" class="nav-link has-dropdown"><i data-feather="send"></i><span>Referral Code Managment</span></a>
				<ul class="dropdown-menu">
					<li><a class="nav-link" href="<?php echo base_url("referral_code"); ?>">Add Referral Code</a></li>
					<li><a class="nav-link" href="<?php echo base_url("view_referral"); ?>">Referral Code List</a></li>

				</ul>
			</li>
			<li class="dropdown"><a href="#" class="nav-link has-dropdown"><i data-feather="send"></i><span>Coupon Code Managment</span></a>
				<ul class="dropdown-menu">
					<li><a class="nav-link" href="<?php echo base_url("add_coupon"); ?>">Add Coupon Code</a></li>
					<li><a class="nav-link" href="<?php echo base_url("view_coupon"); ?>">Coupon Code List</a></li>

				</ul>
			</li>
			<li class="dropdown"><a href="#" class="nav-link has-dropdown"><i data-feather="lock"></i><span>Privacy Policy Managment</span></a>
				<ul class="dropdown-menu">
					<!-- <li><a class="nav-link" href="<?php echo base_url("privacy_policy"); ?>">Privacy Policy </a></li> -->
					<li><a class="nav-link" href="<?php echo base_url("view_privacy"); ?>">View Privacy Policy</a></li>

				</ul>
			</li>
			<li class="dropdown"><a href="#" class="nav-link has-dropdown"><i data-feather="book"></i><span>Terms & Conditon </span></a>
				<ul class="dropdown-menu">
					<!-- <li><a class="nav-link" href="<?php echo base_url("privacy_policy"); ?>">Privacy Policy </a></li> -->
					<li><a class="nav-link" href="<?php echo base_url("view_terms_condition"); ?>">View Tems & Condition</a></li>
				</ul>
			</li>
			<li class="dropdown"><a href="#" class="nav-link has-dropdown"><i data-feather="phone"></i><span>FAQ </span></a>
				<ul class="dropdown-menu">
					<li><a class="nav-link" href="<?php echo base_url("faq"); ?>">Add Faq </a></li>
					<li><a class="nav-link" href="<?php echo base_url("view_faq"); ?>">View Faq</a></li>
				</ul>
			</li>
			<li class="dropdown"><a href="#" class="nav-link has-dropdown"><i data-feather="user"></i><span>Request Manager </span></a>
				<ul class="dropdown-menu">
					<li><a class="nav-link" href="<?php echo base_url("product_list"); ?>">View Product</a></li>
				</ul>
			</li>
			<li class="dropdown"><a href="#" class="nav-link has-dropdown"><i data-feather="user"></i><span>Product Manager </span></a>
				<ul class="dropdown-menu">
					<li><a class="nav-link" href="<?php echo base_url("product_list"); ?>">View Product</a></li>
				</ul>
			</li>

		</ul>
	</aside>
</div>