			<footer class="main-footer">
				<div class="footer-left">
					Copyright &copy; 2021
					<div class="bullet"></div>
					Design By <a href="#">I Work</a>
				</div>
				<div class="footer-right"></div>
			</footer>
			</div>
			</div>
			<!-- General JS Scripts -->
			<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
			<!-- JS Libraies -->
			<script src="assets/bundles/izitoast/js/iziToast.min.js"></script>

			<script src="<?php echo base_url(); ?>assets/bundles/chartjs/chart.min.js"></script>
			<script src="<?php echo base_url(); ?>assets/bundles/apexcharts/apexcharts.min.js"></script>
			<script src="<?php echo base_url(); ?>assets/bundles/jquery.sparkline.min.js"></script>
			<script src="<?php echo base_url(); ?>assets/bundles/jqvmap/dist/jquery.vmap.min.js"></script>
			<script src="<?php echo base_url(); ?>assets/bundles/jqvmap/dist/maps/jquery.vmap.world.js"></script>
			<script src="<?php echo base_url(); ?>assets/bundles/datatables/datatables.min.js"></script>
			<script src="<?php echo base_url(); ?>assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
			<script src="<?php echo base_url(); ?>assets/bundles/jquery-ui/jquery-ui.min.js"></script>
			<script src="http://maps.google.com/maps/api/js?key=AIzaSyBXFAxSgXP7b5D25WEtjxkYqoWM2PjxaLg&callback=initMap&libraries=places"></script>
			<script src="<?php echo base_url(); ?>assets/bundles/gmaps.js"></script>
			<script src="<?php echo base_url(); ?>assets/bundles/summernote/summernote-bs4.js"></script>
			<script src="<?php echo base_url(); ?>assets/bundles/codemirror/lib/codemirror.js"></script>
			<script src="<?php echo base_url(); ?>assets/bundles/codemirror/mode/javascript/javascript.js"></script>
			<script src="<?php echo base_url(); ?>assets/bundles/jquery-selectric/jquery.selectric.min.js"></script>
			<script src="<?php echo base_url(); ?>assets/bundles/ckeditor/ckeditor.js"></script>
			<script src="<?php echo base_url(); ?>assets/bundles/sweetalert/sweetalert.min.js"></script>
			<!-- Page Specific JS File -->
			<script src="<?php echo base_url(); ?>assets/js/page/sweetalert.js"></script>
			<!-- Page Specific JS File -->
			<script src="<?php echo base_url(); ?>assets/js/page/ckeditor.js"></script>
			<!-- Template JS File -->
			<!-- Page Specific JS File -->
			<script src="<?php echo base_url(); ?>assets/js/page/gmaps-marker.js"></script>
			<!-- Page Specific JS File -->
			<script src="<?php echo base_url(); ?>assets/js/page/datatables.js"></script>
			<!-- Page Specific JS File -->
			<script src="<?php echo base_url(); ?>assets/js/page/index2.js"></script>
			<script src="<?php echo base_url(); ?>assets/js/page/todo.js"></script>
			<!-- Template JS File -->
			<script src="<?php echo base_url(); ?>assets/js/scripts.js"></script>
			<!-- Custom JS File -->
			<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.9.0/jquery.validate.min.js" integrity="sha512-FyKT5fVLnePWZFq8zELdcGwSjpMrRZuYmF+7YdKxVREKomnwN0KTUG8/udaVDdYFv7fTMEc+opLqHQRqBGs8+w==" crossorigin="anonymous"></script>
			</body>

			</html>
			