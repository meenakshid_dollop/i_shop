<?php include('includes/header.php'); ?>
<?php include('includes/sidebar.php'); ?>
<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-body">
            <div class="row mt-sm-4">
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Terms & Condition</h4>
                            <a href="<?php echo base_url(); ?>terms_and_condition" class="btn btn-primary" style="position: absolute;right: 20px;">Update Terms </a>
                        </div>
                        <div class="padding-20">
                            <div class="tab-content tab-bordered" id="myTab3Content">
                                <div class="section-title " id="terms"></div>
                                <!-- <p class="m-t-30">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p> -->
                                <!-- <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                    <div class="section-title">Education</div>
                                    <ul>
                                    <li>Lorem Ipsum is simply dummy text</li>
                                    <li>Lorem Ipsum is simply dummy text</li>
                                    <li>Lorem Ipsum is simply dummy text</li>
                                    </ul>
                                    <div class="section-title">Experience</div>
                                    <ul>
                                    <li>Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.
                                    </li>
                                    <li>Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.
                                    </li>
                                    <li>Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.
                                    </li>
                                    <li>Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.
                                    </li>
                                    <li>Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.
                                    </li>
                                    <li>Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.
                                    </li>
                                    </ul> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php include('includes/footer.php'); ?>
<script type="text/javascript">
    var token = '<?php echo $_SESSION['iWorkToken'] ?>';
    $(document).ready(function() {
        var showTerm = "";
        $.ajax({
            url: '<?php echo base_url() ?>getTermsAndCondition',
            type: 'GET',
            dataType: 'json',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': token
            },
            error: function(xhr, status, error) {
                var errorMessage = xhr.status + ': ' + xhr.statusText
                // alert('Error - ' + errorMessage);
            },
            success: function(response) {
                var Terms = response.TermsAndCondition;
              //  console.log(Terms);
                showTerm += " <p>" + Terms.terms + "</p>";
                $('#terms').html(showTerm);

            }
        });


    });
</script>