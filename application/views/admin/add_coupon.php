<?php include('includes/header.php'); ?>
<?php include('includes/sidebar.php'); ?>

<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Add Coupon</h4>
                            <input type="hidden" id="id" value=<?php if (isset($_GET['id'])) {
                                                                    echo $_GET['id'];
                                                                } ?>>
                        </div>
                        <div class="card-body">
                            <form method="POST" name="couponForm" id="couponForm" class="needs-validation" novalidate="">
                                <!-- <div class="form-group row mb-4">
                  <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Offer Title</label>
                  <div class="col-sm-12 col-md-7">
                    <input type="text" class="form-control" id="planTitle" name="planTitle">
                  </div>
                </div> -->
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Coupon Title</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" value="<?php if (isset($coupon['coupon_title']) && $coupon['coupon_title'] != "") {
                                                                        echo $coupon['coupon_title'];
                                                                    } ?>" name="coupon_title" id="coupon_title" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Use Type</label>
                                    <div class="col-sm-12 col-md-7">
                                        <select name="user_type" id="user_type" class="form-control">
                                            <option value="">Select Use type</option>
                                            <option <?php if (isset($coupon['user_type']) && $coupon['user_type'] == "single_use") {
                                                        echo "selected";
                                                    } ?> value="single_use">Single time</option>
                                            <option <?php if (isset($coupon['user_type']) && $coupon['user_type'] != "") {
                                                        echo "selected";
                                                    } ?> value="multi_use">Multi time</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Coupon Type</label>
                                    <div class="col-sm-12 col-md-7">
                                        <select name="couponType" id="couponType" class="form-control">
                                            <option value="">Select Coupon type</option>
                                            <option <?php if (isset($coupon['discount_percent']) && $coupon['discount_percent'] != "") {
                                                        echo "selected";
                                                    } ?> value="percent">Percent</option>
                                            <option <?php if (isset($coupon['discount_amount']) && $coupon['discount_amount'] != "") {
                                                        echo "selected";
                                                    } ?> value="flat">Flat</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Maximum Amount</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="number" name="max_amount" id="max_amount" class="form-control" value="<?php if (isset($coupon['max_amount']) && $coupon['max_amount'] != "") {
                                                                                                                                echo $coupon['max_amount'];
                                                                                                                            } ?>">
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Minimum Amount</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="number" name="min_amount" id="min_amount" class="form-control" value="<?php if (isset($coupon['min_amount']) && $coupon['min_amount'] != "") {
                                                                                                                                echo $coupon['min_amount'];
                                                                                                                            } ?>">
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Start Date</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="date" name="start_date" id="start_date" value="<?php if (isset($coupon['start_date']) && $coupon['start_date'] != "") {
                                                                                                        echo $coupon['start_date'];
                                                                                                    } ?>" class=" form-control">
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">End Date</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="date" name="end_date" id="end_date" value="<?php if (isset($coupon['end_date']) && $coupon['end_date'] != "") {
                                                                                                    echo $coupon['end_date'];
                                                                                                } ?>" class=" form-control">
                                    </div>
                                </div>
                                <div class="discountPercentDiv">
                                    <div class="form-group row mb-4 ">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Discount(%)</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="number" name="discount_percent" value="<?php if (isset($coupon['discount_percent']) && $coupon['discount_percent'] != "") {
                                                                                                    echo $coupon['discount_percent'];
                                                                                                } ?>" id="discount_percent" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="discountAmountDiv">
                                    <div class="form-group row mb-4 ">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Discount Amount</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="number" name="discount_amount" id="discount_amount" value="<?php if (isset($coupon['discount_amount']) && $coupon['discount_amount'] != "") {
                                                                                                                        echo $coupon['discount_amount'];
                                                                                                                    } ?>" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Coupon Code</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" name="coupon_code" id="coupon_code" value="<?php if (isset($coupon['coupon_code']) && $coupon['coupon_code'] != "") {
                                                                                                            echo $coupon['coupon_code'];
                                                                                                        } ?>" class=" form-control">
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Descrption</label>
                                    <div class="col-sm-12 col-md-7">
                                        <textarea class="summernote" id="coupon_descrption" value="<?php if (isset($coupon['coupon_descrption']) && $coupon['coupon_descrption'] != "") {
                                                                                                        echo $coupon['coupon_descrption'];
                                                                                                    } ?>" name="coupon_descrption"><?php if (isset($coupon['coupon_descrption']) && $coupon['coupon_descrption'] != "") {
                                                                                                                                        echo $coupon['coupon_descrption'];
                                                                                                                                    } ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Coupon Image</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="file" name="coupon_image" id="coupon_image" class="form-control">
                                        <img id="blah" src="<?php if (isset($coupon['coupon_image']) && $coupon['coupon_image'] != "") {
                                                                echo $coupon['coupon_image'];
                                                            } else { ?>assets/default.png<?php } ?>" width="200px" height="200px" alt="your image" />

                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                    <div class="col-sm-12 col-md-7">
                                        <button class="btn btn-primary" id="CouponSubmit">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php include('includes/footer.php'); ?>
<script src="assets/bundles/izitoast/js/iziToast.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#blah').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
    }

    $("#coupon_image").change(function() {
        readURL(this);
    });
    $(document).ready(function() {
        $("#couponForm").validate({
            rules: {
                coupon_descrption: 'required',
                max_amount: 'required',
                min_amount: 'required',
                coupon_title: 'required',
                start_date: 'required',
                end_date: 'required',
                coupon_code: 'required',
                // RestrictionType: 'required',
                user_type: 'required',
                Discount: 'required',

            }
        });
    });
</script>
<script type="text/javascript">
    <?php if (isset($coupon['discount_percent']) && $coupon['discount_percent'] != "") { ?>
       // alert("sdjds");
        $(".discountPercentDiv").css('display', "block");
        $(".discountAmountDiv").hide();
    <?php } ?>
    <?php if (isset($coupon['discount_amount']) && $coupon['discount_amount'] != "") { ?>
        $(".discountPercentDiv").hide();
        $(".discountAmountDiv").css('display', "block");;
    <?php } ?>
    $('#couponType').on('change', function() {

        if (this.value == "percent") {
            //alert(this.value);
            $(".discountPercentDiv").show();
            $(".discountAmountDiv").hide();
        } else {
            $(".discountPercentDiv").hide();
            $(".discountAmountDiv").show();
        }

    });
    $(document).ready(function() {
        var token = '<?php echo $_SESSION['iWorkToken'] ?>';
        if (token == "") {
            //window.location.href = "<?php echo base_url(); ?>login";
        } else {

            $("#CouponSubmit").click(function(e) {
                e.preventDefault();
                var coupon_descrption = $('#coupon_descrption').summernote('code');
                var max_amount = $('#max_amount').val();
                var min_amount = $('#min_amount').val();
                var discount_amount = $('#discount_amount').val();
                var discount_percent = $('#discount_percent').val();
                var coupon_title = $('#coupon_title').val();
                var start_date = new Date($('#start_date').val());
                var end_date = new Date($('#end_date').val());
                var coupon_code = $('#coupon_code').val();
                var coupon_id = $('#id').val();
                // var RestrictionType = $('select[name=RestrictionType] option').filter(':selected').val();
                var user_type = $('select[name=user_type] option').filter(':selected').val();
                var file_data = $("#coupon_image").prop("files")[0];
                // console.log(file_data);
                // alert(file_data);
                var form_data = new FormData();
                form_data.append("coupon_image", file_data)
                form_data.append("coupon_descrption", coupon_descrption)
                form_data.append("max_amount", max_amount)
                form_data.append("min_amount", min_amount)
                form_data.append("coupon_title", coupon_title)
                form_data.append("coupon_id", coupon_id)
                form_data.append("discount_percent", discount_percent)
                form_data.append("discount_amount", discount_amount)
                form_data.append("start_date", start_date)
                // form_data.append("percent", Discount)
                form_data.append("end_date", end_date)
                form_data.append("coupon_code", coupon_code)
                // form_data.append("restrictionId", RestrictionType)
                form_data.append("type", user_type)
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>addCoupon",
                    dataType: 'script',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    headers: {
                        'Authorization': token
                    },
                    success: function(res) {
                        <?php if (isset($coupon['coupon_id']) && $coupon['coupon_id'] != "") {
                        ?>
                            iziToast.success({
                                title: 'Coupon',
                                message: 'Coupon Update Successfully',
                                position: 'topRight'
                            });
                        <?php } else { ?>
                            iziToast.success({
                                title: 'Coupon',
                                message: 'Coupon Added Successfully',
                                position: 'topRight'
                            });
                        <?php  } ?>
                    },
                    error: function(res) {

                    }

                });

            });
        }
    });
</script>
<style>
    .discountPercentDiv {
        display: none;
    }

    .discountAmountDiv {
        display: none;
    }
</style>