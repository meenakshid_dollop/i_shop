<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Login - I Work</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="assets/css/app.min.css">
  <!-- <link rel="stylesheet" href="assets/bundles/bootstrap-social/bootstrap-social.css"> -->
  <!-- Template CSS -->
  <link rel="stylesheet" href="assets/css/style.css">
  <link rel="stylesheet" href="assets/css/components.css">
  <!-- Custom style CSS -->
  <link rel="stylesheet" href="assets/css/custom.css">
  <link rel='shortcut icon' type='image/x-icon' href='assets/img/fav_icon.png' />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bundles/izitoast/css/iziToast.min.css">

</head>

<body>
  <div class="loader"></div>
  <div id="app">
    <section class="section">
      <!-- <div class="logo">
        <img src="assets/img/logo_01_fnl.png">
      </div>  -->
      <div class="container mt-5">
        <div class="row">
          <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
            <div class="card card-primary">
              <div class="card-header">
                <h4>Login</h4>
              </div>
              <div class="card-body">
                <form method="POST" name="LoginForm" id="LoginForm" class="needs-validation" novalidate="">
                  <div class="form-group">
                    <label for="email">Email</label>
                    <input id="email" type="email" class="form-control" name="email" tabindex="1" required autofocus>
                    <div class="invalid-feedback">
                      Please fill in your email
                    </div>
                  </div>
                  <div class="form-group">
                    <!-- <div class="d-block">
                      <div class="float-right">
                        <a href="reset-password" class="text-small">
                          Forgot Password?
                        </a>
                      </div>
                    </div> -->
                    <label for="password" class="control-label">Password</label>
                    <input id="password" type="password" class="form-control" name="password" tabindex="2" required>
                    <div class="invalid-feedback">
                      please fill in your password
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="custom-control custom-checkbox">
                      <input type="checkbox" name="remember" class="custom-control-input" tabindex="3" id="remember-me">
                      <label class="custom-control-label" for="remember-me">Remember Me</label>
                    </div>
                  </div>
                  <div class="form-group">
                    <button type="submit" id="loginBtn" onclick="return false;" class="btn btn-primary btn-lg btn-block" tabindex="4">
                      Login
                    </button>
                  </div>
                </form>
              </div>
            </div>

          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- General JS Scripts -->
  <script src="assets/js/app.min.js"></script>
  <!-- JS Libraies -->
  <!-- Page Specific JS File -->
  <!-- Template JS File -->
  <script src="assets/js/scripts.js"></script>
  <!-- Custom JS File -->
  <script src="assets/js/custom.js"></script>
  <script src="assets/bundles/izitoast/js/iziToast.min.js"></script>

  <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>

</body>

</html>
<!-- For admin login -->
<script type="text/javascript">
  $(document).ready(function() {
    $("#LoginForm").validate({
      rules: {
        password: 'required',
        email: {
          required: true,
          email: true,
          maxlength: 255,
        },
      }
    });

    $("#loginBtn").click(function() {
      var email = $("#email").val();
      var password = $("#password").val();
      $.ajax({
        url: '<?php echo base_url() ?>admin_login',
        type: 'POST',
        dataType: 'json',
        data: {
          email: email,
          password: password
        },
        error: function(xhr) {
          console.log(xhr);
          iziToast.error({
            title: 'Login',
            message: xhr.responseJSON.message,
            position: 'topRight'
          });
        },
        success: function(response) {
          iziToast.success({
            title: 'Login',
            message: 'Login success',
            position: 'topRight'
          });
          window.location = "<?php echo base_url(); ?>";
        }
      });
    });

  });
</script>
<style>
  body.dark.dark-sidebar.theme-black {
    background-image: url('assets/img/bg-login.png');
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    height: 90vh;
  }

  label#email-error {
    color: red;
  }

  label#password-error {
    color: red;
  }
</style>