<?php //print_r($_SESSION['ConneKton_session']['admin_token']);die; 
?><?php include('includes/header.php') ?>;
<?php include('includes/sidebar.php') ?>;
<div class="main-content">
    <section class="section">
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Referral Code List</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="Showreferral" class="table table-striped table-hover" id="tableExport" style="width:100%;">
                                    <thead>
                                        <tr>
                                            <th>Referral Code</th>
                                            <th>Active</th>
                                            <th>Delete</th>
                                            <th>Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php include('includes/footer.php') ?>;

<script>
    var token = '<?php echo $_SESSION['iWorkToken'] ?>';
    $(document).ready(function() {
        $.ajax({
            url: "<?php echo base_url('getReferralCode') ?>",
            type: "GET",
            dataType: "json",
            headers: {
                'Authorization': token
            },
            success: function(data) {
                // console.log(data);
                var referral = data.data;
                $.each(referral, function(i, value) {
                    // console.log(value);
                    if (referral[i]['is_active'] == "1") {
                        id = referral[i]['referral_id'];
                        statusChange = 0;
                        var checked = "checked";
                    } else {
                        id = referral[i]['referral_id'];
                        var checked = "";
                        statusChange = 1;
                    }
                    toggle = "<div class='toggle-switch'><label class='toggle-switch switch'  id='swal-6'><input   type='checkbox' " + checked + " onclick='ChangeStatus(" + id + "," + statusChange + ")'   class= 'check " + id + "'   name = 'check' id = 'check'><span  class='slider-switch round'></span> </label></div>";
                    $("#Showreferral tbody:last-child").append(
                        '<tr>' +
                        '<td>' + referral[i]['ref_code'] + '</td>' +
                        "<td>" + toggle + " </td> " +
                        '<td> <a href="#" class="deleteClass" onclick="deleteFunction(' + referral[i]['referral_id'] + ')"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#96a2b4" width="24px" height="24px"><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM8 9h8v10H8V9zm7.5-5l-1-1h-5l-1 1H5v2h14V4z"></path></svg></a>  </td>' +
                        '<td> <a href="<?php echo base_url('referral_code') ?>?id=' + referral[i]['referral_id'] + '"><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"/></svg></a></td>' +
                        '</tr>'
                    );
                });
            },
            error: function(data) {

            }
        });

    });

    function deleteFunction(id) {
        $.ajax({
            url: '<?php echo base_url() ?>referralCode',
            type: 'POST',
            headers: {
                'Authorization': token
            },
            data: {
                isDelete: "deleted",
                ref_code_id: id
            },
            dataType: 'json',
            error: function(response) {
                iziToast.error({
                    title: 'Referral',
                    message: "referral Not Deleted ",
                    position: 'topRight'
                });
            },
            success: function(response) {
                if (response) {
                    iziToast.success({
                        title: 'Referral',
                        message: " referral Deleted Succefully",
                        position: 'topRight'
                    });
                    setTimeout(function() {
                        window.location.reload(1);
                    }, 3000);
                }
            }

        });
    }

    function ChangeStatus(id, status) {
        //alert(id);
        $.ajax({
            url: '<?php echo base_url() ?>referralCode',
            type: 'POST',
            headers: {
                'Authorization': token
            },
            data: {
                isActive: status,
                ref_code_id: id
            },
            dataType: 'json',
            error: function(response) {
                iziToast.error({
                    title: 'Referral',
                    message: "Referral Status Not Change ",
                    position: 'topRight'
                });
            },
            success: function(response) {
                if (response) {
                    iziToast.success({
                        title: 'Referral',
                        message: " Referral Status Change Succefully",
                        position: 'topRight'
                    });
                    setTimeout(function() {
                        window.location.reload(1);
                    }, 3000);
                }
            }

        });
    }
</script>