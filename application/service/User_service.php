<?php

/**
 * @Author:Meenakshi
 *Its Provide services
 */
class User_service extends MY_Service
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('AuthModel');
    $this->load->model('Common_model');
    
  }
  ///////////////for user login
  public function userLogin($mobile,$password)
  {
    $where  = array('mobile' => $mobile , 'password' => $password);
    $result = $this->Common_model->getDataWhere('user', '*', $where);
    return $result;
  }
  ////////////////////////Create New user
  public function createUser($userData)
  {
    return $this->Common_model->insertData("user", $userData);
  }
  ///////////////////For Update User By Id
  public function updateUserById($id, $userData)
  {
    $where  = array('user_id' => $id);
    $result = $this->Common_model->updateData('user', $where, $userData);
    return $result;
  }
  ///////////////////////Match Otp 

  public function matchOtp($mobileNo, $otp)
  {
    $where  = array('mobile' => $mobileNo, 'otp' => $otp);
    $result = $this->Common_model->getDataWhere('user', '*,', $where);
    //print_r($mobileNo);die;
    return $result;
  }

  //////////////////////check unique mail 
  public function CheckMail($email)
  {
    $where  = array('user_email' => $email, 'is_active '=>"1" ,'is_delete'=>"not_deleted");
    $result = $this->Common_model->getDataWhere('user', '*', $where);
    //print_r($this->db->last_query());die;
    return $result;
  }
  
    //////////////////////check unique mobile 
    public function checkMobile($mobile)
    {
      $where  = array('mobile' => $mobile, 'is_active '=>"1" ,'is_delete'=>"not_deleted");
      $result = $this->Common_model->getDataWhere('user', '*', $where);
      return $result;
    }
    ///////////////get user by id
    public function getUserById($id)
    {
      $where  = array('user_id' => $id, 'is_active '=>"1" ,'is_delete'=>"not_deleted");
      $result = $this->Common_model->getDataWhere('user', '*', $where);
      return $result;
    }
  ///////add categories
  public function addCategories($data)
  {
    return  $this->Common_model->insertData('categories', $data);
  }
  public function addSubCategories($data)
  {
    return  $this->Common_model->insertData('sub_category', $data);
  }
  //////////add sub category

  //////////////check categories name
  public function checkNameCategories($categoryName)
  {
    $sql = "SELECT * FROM `categories` WHERE category_name = '$categoryName'  AND is_delete = 'not_deleted' ";
    $result = $this->Common_model->executeSql($sql, "row_array");
    return $result;
  }
  ///////check sub category

  public function checkNameSubCategories($subcategoryName, $MaincategoryId)
  {
    $sql = "SELECT * FROM `sub_category` WHERE sub_category_name = '$subcategoryName' AND category_id = '$MaincategoryId' AND is_delete = 'not_deleted' ";
    $result = $this->Common_model->executeSql($sql, "row_array");
    return $result;
  }
  //////update category
  public function updateCategory($categoryId, $data)
  {
    $where = array('category_id' => $categoryId);
    return $this->Common_model->updateData('categories', $where, $data);
  }
  ////////////update sub category
  public function updateSubCategory($subcategoryId, $data)
  {
    $where = array('sub_category_id' => $subcategoryId);
    return $this->Common_model->updateData('sub_category', $where, $data);
  }

  ///////////////////////get all category

  public function getCategories()
  {
    $sql = "SELECT * FROM `categories` WHERE  is_delete = 'not_deleted' ";
    $result = $this->Common_model->executeSql($sql, "result_array");
    return $result;
  }
  /////////////////get category by id
  public function getCategorieyById($catId)
  {
    $sql = "SELECT * FROM `categories` WHERE  is_delete = 'not_deleted' AND category_id = '$catId' ";
    $result = $this->Common_model->executeSql($sql, "row_array");
    return $result;
  }
  ///////////////////////get all category

  public function getSubCategories()
  {
    $sql = "SELECT sub_category.*,categories.category_id,categories.category_name FROM `sub_category`
    LEFT JOIN categories ON categories.category_id = sub_category.category_id
    WHERE sub_category.is_delete = 'not_deleted'";
    $result = $this->Common_model->executeSql($sql, "result_array");
    return $result;
  }
  /////////////////get category by id
  public function getSubCategorieyById($subcatId)
  {
    $sql = "SELECT * FROM `sub_category` WHERE  is_delete = 'not_deleted' AND sub_category_id = '$subcatId' ";
    $result = $this->Common_model->executeSql($sql, "row_array");
    return $result;
  }
  public function getSubCategoriesByUser($CatId)
  {
    $sql = "SELECT sub_category.*,categories.category_name 
            FROM `sub_category` 
            JOIN categories ON categories.category_id = sub_category.category_id 
            WHERE sub_category.is_delete = 'not_deleted' 
            AND sub_category.category_id = '$CatId' AND sub_category.is_active=1";
    $result = $this->Common_model->executeSql($sql, "result_array");
    return $result;
  }
  ////////////////////update referral code
  public function updateRefferalCode($ReferralCodeId, $data)
  {
    $where = array('referral_id' => $ReferralCodeId);
    return $this->Common_model->updateData('referral_code', $where, $data);
  }
  ////////////////////check referraalcode
  public function checkRefferalCode($ReferralCode)
  {
    $sql = "SELECT * FROM `referral_code` WHERE ref_code = '$ReferralCode'  AND is_delete = 'not_deleted' ";
    $result = $this->Common_model->executeSql($sql, "row_array");
    return $result;
  }
  //////////////add  referral code
  public function addRefferalCode($data)
  {
    return  $this->Common_model->insertData('referral_code', $data);
  }
  /////////////////get referral code by id
  public function getReferralCodeById($refId)
  {
    $sql = "SELECT * FROM `referral_code` WHERE  is_delete = 'not_deleted' AND referral_id = '$refId' ";
    $result = $this->Common_model->executeSql($sql, "row_array");
    return $result;
  }
  /////////////////////get all referral code 
  public function getReferralCode()
  {
    $sql = "SELECT * FROM `referral_code` WHERE  is_delete = 'not_deleted' ";
    $result = $this->Common_model->executeSql($sql, "result_array");
    return $result;
  }
  ////////////////////admin login
  public function adminLogin($email,$password)
  {
    $password = md5($password);
    $sql = "SELECT * FROM `admin` WHERE  admin_email = '$email' AND admin_password='$password'";
    $result = $this->Common_model->executeSql($sql, "row_array");
    return $result;
  }
  //////////////////check coupon code
  public function CheckcouponCode($coupon_code)
  {
    $sql = "SELECT * FROM `coupon` WHERE  coupon_code = '$coupon_code' AND is_delete='not_deleted'";
    $result = $this->Common_model->executeSql($sql, "row_array");
    return $result;
  }
  ///////////////add coupon 
  public function addCoupon($coupon_data)
  {
    return  $this->Common_model->insertData('coupon', $coupon_data);
  }
  //////////////////update coupon 
  public function updateCoupon($coupon_id, $data)
  {
    $where = array('coupon_id' => $coupon_id);
    return $this->Common_model->updateData('coupon', $where, $data);
  }
  /////////////////get all coupon
  public function getCoupon()
  {
    $sql = "SELECT * FROM `coupon` WHERE  is_delete='not_deleted'";
    $result = $this->Common_model->executeSql($sql, "result_array");
    return $result;
  }
  /////////////////get coupon by id
  public function getCouponById($couponId)
  {
    $sql = "SELECT * FROM `coupon` WHERE coupon_id = '$couponId' AND is_delete = 'not_deleted'";
    $result = $this->Common_model->executeSql($sql, "row_array");
    return $result;
  }
  ////////////get all user
  public function getUser()
  {
    $sql = "SELECT * FROM `user` WHERE is_delete = 'not_deleted'";
    $result = $this->Common_model->executeSql($sql, "result_array");
    return $result;
  }
  /////////////get user by admin by id
  public function getSingleUserByAdmin($userID)
  {
    $sql = "SELECT * FROM `user` WHERE user_id = '$userID' AND is_delete = 'not_deleted'";
    $result = $this->Common_model->executeSql($sql, "row_array");
    return $result;
  }
  //////////////////////update privcy policy
  
  public function updatePolicy($data, $policy_id)
  {
    $where = array('id' => $policy_id);
    return $this->Common_model->updateData('privcy_policy', $where, $data);
  }
  ///////////get policy 
  public function getPolicy()
  {
    $sql = "SELECT * FROM `privcy_policy`";
    $result = $this->Common_model->executeSql($sql, "row_array");
    return $result;
  }
  //////////////////////////add terms and condition
  public function updateTerms($terms_id,$data)
  {
    $where = array('id' => $terms_id);
    return $this->Common_model->updateData('terms_and_conditon', $where, $data);
  }
  //////////////////get terms condtion
  public function GetTermsAndCondition()
  {
    $sql = "SELECT * FROM `terms_and_conditon`";
    $result = $this->Common_model->executeSql($sql, "row_array");
    return $result;
  }
  /////////////////update faq
  public function  updateFaq($updatefaq, $faq_id)
  {
    $where = array('id' => $faq_id);
    return $this->Common_model->updateData('faq', $where, $updatefaq);
  }
  //////////////////////////add faq
  public function addFaq($addfaq)
  {
    return  $this->Common_model->insertData('faq', $addfaq);
  }
  ////////////delete faq
  public function deleteFaq($faq_id)
  {
    $where = array('id' => $faq_id,);
    return $this->Common_model->deleteData('faq', $where);
  }
  /////////////////get faq
  public function getFaq()
  {
    $sql = "SELECT * FROM `faq`";
    $result = $this->Common_model->executeSql($sql, "result_array");
    return $result;
  }
  /////////////////////get faq by id
  public function getFaqById($faq_id)
  {
    $sql = "SELECT * FROM `faq` WHERE id ='$faq_id'";
    $result = $this->Common_model->executeSql($sql, "row_array");
    return $result;
  }
///////////////////////////contact us
public function contactUs($conatctUsData)
{
    return  $this->Common_model->insertData('contact_us', $conatctUsData);

}
//////////////check Admin Password
public function checkPassword($oldPassword)
{
    $oldPassword = md5($oldPassword);
    $sql = "SELECT * FROM `admin` WHERE admin_password ='$oldPassword'";
    $result = $this->Common_model->executeSql($sql, "row_array");
    return $result;
}
/////////////change password
public function changePassword($new_password)
{
    $where = array('admin_id' =>1);
    $updatePassword =  array('admin_password' => md5($new_password) );
    return $this->Common_model->updateData('admin', $where, $updatePassword);
}
//////////////////////update request 
public function updateRequest($request_id,$updateRequestData)
{
    $where = array('request_id' => $request_id);   
    return $this->Common_model->updateData('request', $where, $updateRequestData);
}
////////////////////add request
public function addRequest($data)
{
    return  $this->Common_model->insertData('request', $data);
}
////////////////////get alll poduct and request
public function getAllRequest()
{
    $sql = "SELECT request.*,(SELECT COUNT(*) FROM bid WHERE bid.request_id = request.request_id)  AS TotalBid FROM `request`  WHERE request.is_delete = 'not_deleted'";
    $result = $this->Common_model->executeSql($sql, "result_array");
    return $result;
}
////////////////////////////get request  by id
public function getRequestById($request_id)
{
    $sql = "SELECT request.*,(SELECT COUNT(*) FROM bid WHERE bid.request_id = request.request_id)  AS TotalBid FROM `request`  WHERE request.request_id = '$request_id' AND request.is_delete = 'not_deleted'";
    $result = $this->Common_model->executeSql($sql, "row_array");
    return $result;
}
  /////////////////////////////////////add bid
  public function addBid($data)
  {
    return  $this->Common_model->insertData('bid', $data);
  }
  //////////////////////////////update bid
  public function updateBid($bid_id,$updateData)
  {
    $where = array('bid_id' => $bid_id);
 //  print_r($bid_id);die;
    return $this->Common_model->updateData('bid', $where, $updateData);
  }
///////////////////////////////get bid
public function getAllBidByRequestId($request_id)
{
    $sql = "SELECT bid.*,user.user_name,user.user_profile,request.created_at AS requestDate,request.request_title,request.request_sub_title,request.request_image,request.request_amount,
            DATE_FORMAT(bid.created_at,'%d %b%,%Y') AS bid_day 
            FROM bid 
            LEFT JOIN user ON user.user_id = bid.user_id 
            LEFT JOIN request ON request.request_id = bid.request_id 
            WHERE bid.request_id = '$request_id' AND bid.bid_status = 'accept' AND bid.is_delete ='not_deleted' ORDER BY bid.bid_amount DESC ";
    $result['accept'] = $this->Common_model->executeSql($sql, "result_array");
    $sql = "SELECT bid.*,user.user_name,user.user_profile,request.created_at AS requestDate,request.request_title,request.request_sub_title,request.request_image,request.request_amount,
            DATE_FORMAT(bid.created_at,'%d %b%,%Y') AS bid_day 
            FROM bid 
            LEFT JOIN user ON user.user_id = bid.user_id 
            LEFT JOIN request ON request.request_id = bid.request_id 
            WHERE bid.request_id = '$request_id' AND bid.bid_status = 'pending' AND bid.is_delete ='not_deleted' ORDER BY bid.bid_amount DESC ";
    $result['pending'] = $this->Common_model->executeSql($sql, "result_array");
    $sql = "SELECT bid.*,user.user_name,user.user_profile,request.created_at AS requestDate,request.request_title,request.request_sub_title,request.request_image,request.request_amount,
            DATE_FORMAT(bid.created_at,'%d %b%,%Y') AS bid_day 
            FROM bid 
            LEFT JOIN user ON user.user_id = bid.user_id 
            LEFT JOIN request ON request.request_id = bid.request_id 
            WHERE bid.request_id = '$request_id' AND bid.bid_status = 'complete' AND bid.is_delete ='not_deleted' ORDER BY bid.bid_amount DESC ";
    $result['complete'] = $this->Common_model->executeSql($sql, "result_array");
    $sql = "SELECT bid.*,user.user_name,user.user_profile,request.created_at AS requestDate,request.request_title,request.request_sub_title,request.request_image,request.request_amount,
            DATE_FORMAT(bid.created_at,'%d %b%,%Y') AS bid_day 
            FROM bid 
            LEFT JOIN user ON user.user_id = bid.user_id 
            LEFT JOIN request ON request.request_id = bid.request_id 
            WHERE bid.request_id = '$request_id' AND bid.bid_status = 'cancel' AND bid.is_delete ='not_deleted' ORDER BY bid.bid_amount DESC ";
    $result['cancel'] = $this->Common_model->executeSql($sql, "result_array");
    return $result;
}
  /////////////////get user bid
  
  public function getUserBid($userId)
  {
    $sql = "SELECT bid.*,user.user_name,user.user_profile,request.created_at AS requestDate,request.request_title,request.request_sub_title,request.request_image,request.request_amount,
            DATE_FORMAT(bid.created_at,'%d %b%,%Y') AS bid_day 
            FROM bid 
            LEFT JOIN user ON user.user_id = bid.user_id 
            LEFT JOIN request ON request.request_id = bid.request_id 
            WHERE bid.user_id = '$userId' AND bid.is_delete ='not_deleted' ORDER BY bid.bid_amount DESC";
    $result = $this->Common_model->executeSql($sql, "result_array");
    return $result;
  }
public function checkBid($request_id, $id)
{
    $sql = "SELECT *  FROM `bid`  WHERE request_id = '$request_id' AND user_id ='$id' AND is_delete = 'not_deleted'";
    $result = $this->Common_model->executeSql($sql, "row_array");
    return $result;
   
}
public function getUserProfile($userId)
{
    $sql = "SELECT user_name,user_profile,mobile FROM `user` WHERE is_delete = 'not_deleted' AND is_active= 1 AND user_id = '$userId'";
    $result = $this->Common_model->executeSql($sql, "result_array");
    return $result;
}
  public function upload_multiple_images($files)
  {
    // print_r($files);die;
    $image_path = "";
    if (isset($files["tmp_name"]) && $files["tmp_name"][0] != '') {
      $extension = array("jpeg", "jpg", "png", "gif");
      $i = 0;
      
      foreach ($files["tmp_name"] as $key => $tmp_name) {
        $file_name = $files["name"][$key];
        $file_tmp = $files["tmp_name"][$key];
        $ext = pathinfo($file_name, PATHINFO_EXTENSION);

        $filename = basename($file_name, $ext);
        $newFileName = md5($filename . time()) . "$i." . $ext;
        $fullpath = "upload/product/" . $newFileName;
        move_uploaded_file($file_tmp = $files["tmp_name"][$key], $fullpath);
        $image_path .= "," . $fullpath;
        $i++;
      }
    }
    return ltrim($image_path, ",");
  }
public function getAllOfferByUser()
{
  # code...
}
public function getAllOfferById()
{
  # code...
}
  
}