<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 *@Author:Meenakshi
 *This controller working for view load admin pages  opration
 */
class DefaultController extends CI_Controller
{
	function __construct()

	{
		parent::__construct();
		 
		   $this->config->load('myConstant');
	        $this->load->helper(array('form', 'url', 'Validation_helper'));
	        $this->load->library('form_validation');
	        $this->load->database('');
	        $this->load->service('User_service');
	        $this->load->library('Authorization_Token');
	        $this->load->helper('date');
	        date_default_timezone_set("Asia/Kolkata");
	

	}
	
	public function index()
	{
		$data	=	array();
		if (isset($_SESSION['iWorkToken']) && $_SESSION['iWorkToken'] != "") {
		$this->load->view('admin/dashboard', $data);
		} else {
			$this->session->set_flashdata('error', 'You must login ');
			redirect(base_url() . 'login');
		}
	}

	public function category()
	{
		$data = array();
		if (isset($_SESSION['iWorkToken']) && $_SESSION['iWorkToken'] != "") 
		{
			if(isset($_GET['id']) && $_GET['id'] !="")
			{
				$id = $_GET['id'];
				//print_r($id);die;
				$data['getCategoryById'] = $this->user_service->getCategorieyById($id);
			}
			$this->load->view('admin/category',$data);
		} 
		else {			
			$this->session->set_flashdata('error', 'You must login ');
			redirect(base_url() . 'login');
		}
		
	}
	public function login()
	{
		if (isset($_SESSION['iWorkToken']) && $_SESSION['iWorkToken'] != "") {
			redirect(base_url());
		}else{
		$this->load->view('admin/login');
		}
	}
	public function subcategory()
	{
		if (isset($_SESSION['iWorkToken']) && $_SESSION['iWorkToken'] != "")
		{
			$data = array();
			$data['category'] =	$this->user_service->getCategories();
			if (isset($_GET['id']) && $_GET['id'] != "") {
				$id = $_GET['id'];
				//	print_r($id);die;
				$data['getSubCategoryById'] = $this->user_service->getSubCategorieyById($id);
			}
		//	print_r($data['getSubCategoryById']);die;
			$this->load->view('admin/subcategory', $data);
		}else {
			redirect(base_url() . 'login');
		}
	}
	public function view_subcategory()
	{
		if (isset($_SESSION['iWorkToken']) && $_SESSION['iWorkToken'] != "") {
			$this->load->view('admin/view_subcategory');
		} else {
			redirect(base_url() . 'login');
		}
		
	}
	public function view_category()
	{
		if (isset($_SESSION['iWorkToken']) && $_SESSION['iWorkToken'] != "") {
			$this->load->view('admin/view_category');
		} else {
			redirect(base_url() . 'login');
		}	
		
	}
	public function referral_code()
	{
		$data = array();
		if (isset($_SESSION['iWorkToken']) && $_SESSION['iWorkToken'] != "") {
			if (isset($_GET['id']) && $_GET['id'] != "") {
				$id = $_GET['id'];
				//	print_r($id);die;
				$data['getReferralCodeById'] = $this->user_service->getReferralCodeById($id);
			}
			$this->load->view('admin/referral_code',$data);
		} else {
			redirect(base_url() . 'login');
		}
		
	}
	public function view_referral_code()
	{
		if (isset($_SESSION['iWorkToken']) && $_SESSION['iWorkToken'] != "") {
			$this->load->view('admin/view_referral_code');
		} else {
			redirect(base_url() . 'login');
		}
		
	}
	////////////////////for logout admin
	public function logout()
	{
		//print_r($_SESSION);die;
		unset($_SESSION['iWorkToken']);	
		//$this->session->set_flashdata('success', 'Logout Successfully');
		redirect(base_url(''));
	}
	//////////////////// 
	public function add_coupon()
	{
		if (isset($_SESSION['iWorkToken']) && $_SESSION['iWorkToken'] != "") {
			$data  = array();
			if(isset($_GET['id']) && $_GET['id'] !="")
			{
				$id = $_GET['id'];
				$data['coupon'] = $this->user_service->getcouponById($id);
			}
			$this->load->view('admin/add_coupon', $data);
		} else {
			redirect(base_url() . 'login');
		}
	}
	public function view_coupon()
	{
		if (isset($_SESSION['iWorkToken']) && $_SESSION['iWorkToken'] != "") {
			$this->load->view('admin/view_coupon');
		} else {
			redirect(base_url() . 'login');
		}
	}
	public function view_user()
	{
		if (isset($_SESSION['iWorkToken']) && $_SESSION['iWorkToken'] != "") {
			$this->load->view('admin/view_user');
		} else {
			redirect(base_url() . 'login');
		}
	}
	public function view_privacy()
	{
		if (isset($_SESSION['iWorkToken']) && $_SESSION['iWorkToken'] != "") {
			$this->load->view('admin/view_privacy');
		} else {
			redirect(base_url() . 'login');
		}
	}
	public function privacy_policy()
	{
		if (isset($_SESSION['iWorkToken']) && $_SESSION['iWorkToken'] != "") {
			$data  = array();			
				$data['policy'] = $this->user_service->getPolicy();
			
			$this->load->view('admin/privacy_policy', $data);
		} else {
			redirect(base_url() . 'login');
		}
	}
	public function view_terms_condition()
	{
		if (isset($_SESSION['iWorkToken']) && $_SESSION['iWorkToken'] != "") {
			$data  = array();			
				$data['policy'] = $this->user_service->getPolicy();
			
			$this->load->view('admin/view_terms_condition', $data);
		} else {
			redirect(base_url() . 'login');
		}
	}
	public function terms_and_condition()
	{
		if (isset($_SESSION['iWorkToken']) && $_SESSION['iWorkToken'] != "") {
			$data  = array();			
				$data['terms'] = $this->user_service->getTermsAndCondition();
			
			$this->load->view('admin/terms_and_condition', $data);
		} else {
			redirect(base_url() . 'login');
		}
	}
	public function view_faq()
	{
		if (isset($_SESSION['iWorkToken']) && $_SESSION['iWorkToken'] != "") {
			$data  = array();			
				$data['faq'] = $this->user_service->getFaq();
			
			$this->load->view('admin/view_faq', $data);
		} else {
			redirect(base_url() . 'login');
		}
	}
	public function faq()
	{
		if (isset($_SESSION['iWorkToken']) && $_SESSION['iWorkToken'] != "") {
			$data  = array();	
			if(isset($_GET['id']) && $_GET['id'] !="")
			{
				$data['faqById'] = $this->user_service->getFaqById($_GET['id']);
			}	
			
			$this->load->view('admin/faq', $data);
		} else {
			redirect(base_url() . 'login');
		}
	}
	public function change_password()
	{
		if (isset($_SESSION['iWorkToken']) && $_SESSION['iWorkToken'] != "") {
		//	$data  = array();	
			$this->load->view('admin/change_password');
		} else {
			redirect(base_url() . 'login');
		}
	}
	public function product_list()
	{
		if (isset($_SESSION['iWorkToken']) && $_SESSION['iWorkToken'] != "") {
		//	$data  = array();	
			$this->load->view('admin/product_list');
		} else {
			redirect(base_url() . 'login');
		}
	}
	public function request_list()
	{
		if (isset($_SESSION['iWorkToken']) && $_SESSION['iWorkToken'] != "") {
		//	$data  = array();	
			$this->load->view('admin/request_list');
		} else {
			redirect(base_url() . 'login');
		}
	}
	
}

?>