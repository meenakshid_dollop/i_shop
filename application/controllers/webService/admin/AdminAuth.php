<?php
require APPPATH . 'libraries/REST_Controller.php';
/**
 *@Author:Meenakshi
 *This controller working for user opration
 */
class AdminAuth extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->config->load('myConstant');
        $this->load->library('session');
        $this->load->library('Authorization_Token');
        $this->load->helper(array('form', 'url', 'Validation_helper'));
        $this->load->library('form_validation');
        $this->load->database('');
        $this->load->service('User_service');
    }    



    /* 
	Method : POST
	Use : For User Login
	Param : Email(String)
			Password (String)
	Response : OK
*/
    public function admin_login_post()
    {
        $email = $this->input->post('email');
        $password = $this->input->post('password');        
        if ((isset($email) && $email != "") &&  (isset($password) && $password != "")) {
            $adminLogin = $this->user_service->adminLogin($email,$password);
            if ($adminLogin) 
            {
                $userData['role'] = "Admin";
                $userData['id'] = $adminLogin['admin_id'];
                $tokenData = $this->authorization_token->generateToken($userData);
                $token = $tokenData;
                $this->session->set_userdata('iWorkToken', $token);
              //  $this->session->set_userdata('eamil', $email);
                $this->response(array("message" => MESSAGE_conf::SUCCESS, "iWorkToken" => $token), REST_Controller::HTTP_OK);
            }   
           
            else {
                $this->response(array("message" => MESSAGE_conf::INVALID_DETAILS), REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            $this->response(array("message" => MESSAGE_conf::ALL_REQUIRED), REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    

   
   
}
