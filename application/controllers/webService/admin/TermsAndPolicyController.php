<?php
require APPPATH . 'libraries/REST_Controller.php';
defined('BASEPATH') or exit('No direct script access allowed');
// @ this controller used for terms and condtion privacy policy and faq module
////@ Author: Meenakshi

class TermsAndPolicyController extends REST_Controller
{
    function __construct()

    {
        parent::__construct();

        $this->config->load('myConstant');
        $this->load->helper(array('form', 'url', 'Validation_helper'));
        $this->load->library('form_validation');
        $this->load->database('');
        $this->load->service('User_service');
        $this->load->library('Authorization_Token');
        $this->load->helper('date');
        date_default_timezone_set("Asia/Kolkata");
    }
    /*
	@Use :  Add terms and condition
    @Method : Post
    @Param:termsAndPolicy : Type:String
	@respose : Ok
       */
    public function updatePolicy_post()
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if (isset($result)) {
            $role = $result['role'];
            if ($role == 'Admin') {
                $policy = $this->post('policy');
                $policy_id = $this->post('policy_id');
                if (isset($policy)  &&  $policy != "" && isset($policy_id)  &&  $policy_id != "") {
                    $policy = array(
                        'policy' => $policy,
                    );
                    $result =  $this->user_service->updatePolicy($policy, $policy_id);
                    if ($result) {
                        $this->response(array("message" => MESSAGE_conf::SUCCESS), REST_Controller::HTTP_OK);
                    } else {
                        $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_BAD_REQUEST);
                    }
                } else {
                    $this->response(array("message" => MESSAGE_conf::ALL_REQUIRED), REST_Controller::HTTP_BAD_REQUEST);
                }
            } else {
                $this->response(array("message" => MESSAGE_conf::UNAUTH), REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            return $result;
        }
    }
    /**
     * Use: Get terms and policy
     * Method : Get
     * Response:Ok
     */
    public function getPolicy_get()
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if (isset($result)) {
            $role = $result['role'];
            if ($role == 'Admin') {
                $result2 =  $this->user_service->getPolicy();
                if ($result2) {
                    $this->response(array("message" => MESSAGE_conf::SUCCESS, "policy" => $result2), REST_Controller::HTTP_OK);
                } else {
                    $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_BAD_REQUEST);
                }
            } else {
                $this->response(array("message" => MESSAGE_conf::UNAUTH), REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            $result2 =  $this->user_service->getPolicy();
            if ($result2) {
                $this->response(array("message" => MESSAGE_conf::SUCCESS, "policy" => $result2), REST_Controller::HTTP_OK);
            } else {
                $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_BAD_REQUEST);
            }
            // return $result;
        }
    }
    /*
	@Use :  Add terms and condition
    @Method : Post
    @Param:termsAndPolicy : Type:String
	@respose : Ok
       */
    public function updateTermsAndPolicy_post()
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if (isset($result)) {
            $role = $result['role'];
            if ($role == 'Admin') {
                $terms = $this->post('termsAndPolicy');
                $terms_id = $this->post('terms_id');
                if (isset($terms)  &&  $terms != "") {
                    $termsDetail = array(
                        'terms' => $terms,
                        'updated_at' => date('Y:m:d H:i:s')
                    );
                    $result =  $this->user_service->updateTerms($terms_id,$termsDetail);
                    if ($result) {
                        $this->response(array("message" => MESSAGE_conf::SUCCESS), REST_Controller::HTTP_OK);
                    } else {
                        $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_BAD_REQUEST);
                    }
                } else {
                    $this->response(array("message" => MESSAGE_conf::ALL_REQUIRED), REST_Controller::HTTP_BAD_REQUEST);
                }
            } else {
                $this->response(array("message" => MESSAGE_conf::UNAUTH), REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            return $result;
        }
    }
    /**
     * Use: Get terms and policy
     * Method : Get
     * Response:Ok
     */
    public function getTermsAndCondition_get()
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if (isset($result)) {
            $role = $result['role'];
            if ($role == 'Admin' ) {
                $GetTermsAndCondition =  $this->user_service->GetTermsAndCondition();
                if ($GetTermsAndCondition) {
                    $this->response(array("message" => MESSAGE_conf::SUCCESS, "TermsAndCondition" => $GetTermsAndCondition), REST_Controller::HTTP_OK);
                } else {
                    $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_BAD_REQUEST);
                }
            } else {
                $this->response(array("message" => MESSAGE_conf::UNAUTH), REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            $GetTermsAndCondition =  $this->user_service->GetTermsAndCondition();
            if ($GetTermsAndCondition) {
                $this->response(array("message" => MESSAGE_conf::SUCCESS, "TermsAndCondition" => $GetTermsAndCondition), REST_Controller::HTTP_OK);
            } else {
                $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_BAD_REQUEST);
            }
            //return $result;
        }
    }
    /**
     * Use: Add FAQ's by admin
     * Method : POST
     * Param:title Type: String
        *content Type: String
     *Response : Ok
     */
    public function addFAQByAdmin_post()
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if (!empty($result)) {
            $role = $result['role'];
            if ($role == 'Admin') {
                $title = $this->post('title');
                $content = $this->post('content');
                $faq_id = $this->post('faq_id');
                if (isset($faq_id) && $faq_id != "") {
                    $faq['content'] = json_encode($content);
                    $faq['title'] = json_encode($title);
                    $data = json_encode($faq);
                    $updatefaq = array('content' => $data);
                    $result = $this->user_service->updateFaq($updatefaq, $faq_id);
                    if ($result) {
                        $this->response(array("message" => MESSAGE_conf::SUCCESS), REST_Controller::HTTP_OK);
                    } else {
                        $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_BAD_REQUEST);
                    }
                } else {
                    if ($title && $content) {
                        $faq['content'] = json_encode($content);
                        $faq['title'] = json_encode($title);
                        $data = json_encode($faq);
                        $addfaq = array('content' => $data);
                        $result = $this->user_service->addFaq($addfaq);
                        if ($result) {
                            $this->response(array("message" => MESSAGE_conf::SUCCESS), REST_Controller::HTTP_OK);
                        } else {
                            $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_FORBIDDEN);
                        }
                    } else {
                        $this->response(array("message" => MESSAGE_conf::ALL_REQUIRED), REST_Controller::HTTP_FORBIDDEN);
                    }
                }
            } else {
                $this->response(array("message" => MESSAGE_conf::UNAUTH), REST_Controller::HTTP_FORBIDDEN);
            }
        } else {
            return $result;
        }
    }
    /**
     * Use: GET FAQ's by admin
     * Method : GET
     *Response : Ok
     */
    public function getFaq_get($faq_id = "")
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if (isset($result)) {
            $role = $result['role'];
            if ($role == 'Admin') {
                if ($faq_id == "") {
                    $Getfaq = $this->user_service->getFaq();
                } else {
                    $Getfaq = $this->user_service->getFaqById($faq_id);
                }
                if ($Getfaq) {
                    $this->response(array("message" => MESSAGE_conf::SUCCESS, "faq" => $Getfaq), REST_Controller::HTTP_OK);
                } else {
                    $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_BAD_REQUEST);
                }
            } else {
                $this->response(array("message" => MESSAGE_conf::UNAUTH), REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            $Getfaq = $this->user_service->getFaq();
            if ($Getfaq) {
                $this->response(array("message" => MESSAGE_conf::SUCCESS, "faq" => $Getfaq), REST_Controller::HTTP_OK);
            } else {
                $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_BAD_REQUEST);
            }

            // return $result;
        }
    }

    /***
    use:delete faq
    Method:delete
    Param:Faq id(Int)
    Response:Ok
     *****/
    public function deleteFaq_delete($faq_id = "")
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if (isset($result)) {
            $role = $result['role'];
            if ($role == 'Admin')
             {
                if ($faq_id != "") {
                    $deleteFaq = $this->user_service->deleteFaq($faq_id);
                    if ($deleteFaq) {
                        $this->response(array("message" => MESSAGE_conf::SUCCESS), REST_Controller::HTTP_OK);
                    } else {
                        $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_BAD_REQUEST);
                    }
                } else {
                    $this->response(array("message" => MESSAGE_conf::ALL_REQUIRED), REST_Controller::HTTP_FORBIDDEN);
                }
            }
            else 
            {
                $this->response(array("message" => MESSAGE_conf::UNAUTH), REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            return $result;
        }
    }
}
