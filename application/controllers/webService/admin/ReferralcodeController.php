<?php
require APPPATH . 'libraries/REST_Controller.php';
defined('BASEPATH') or exit('No direct script access allowed');
// @ use for the admin for referral code functionality
/////@ Author: Meenakshi
class ReferralcodeController extends REST_Controller
{
    function __construct()

    {
        parent::__construct();

        $this->config->load('myConstant');
        $this->load->helper(array('form', 'url', 'Validation_helper'));
        $this->load->library('form_validation');
        $this->load->database('');
        $this->load->service('User_service');
        $this->load->library('Authorization_Token');
        $this->load->helper('date');
        date_default_timezone_set("Asia/Kolkata");
    }
    /*****
     * Use:add Referral Code 
     * Method:post
     * Param : ReferralCode Name Type : String
     *         isActive Type Tinyint = 0/1
     * Response : OK****** */
    public function referralCode_post()
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if (isset($result)) {
            $role = $result['role'];
            if($role="Admin")
            {
                $ref_code = $this->input->post('ref_code');
                $isActive = $this->input->post('isActive');
                $isDelete = $this->input->post('isDelete');
                $ref_code_id = $this->input->post('ref_code_id');
                if ($ref_code_id != "") {
                    if ($ref_code != "") {
                        $data['ref_code'] = $ref_code;
                    }
                    if ($isActive != "") {
                        $data['is_active'] = $isActive;
                    }
                    if ($isDelete != "") {
                        $data['is_delete'] = $isDelete;
                    }
                    $updateRefferalCode = $this->user_service->updateRefferalCode($ref_code_id, $data);
                    if ($updateRefferalCode) {
                        $this->response(array("message" => MESSAGE_conf::UPDATE_REFERRALCODE), REST_Controller::HTTP_OK);
                    } else {
                        $this->response(array("message" => MESSAGE_conf::REFERRALCODE_UPDATE_FAILED), REST_Controller::HTTP_BAD_REQUEST);
                    }
                } else {
                    if ($ref_code != "") {
                        $checkRefferalCode = $this->user_service->checkRefferalCode($ref_code);
                        // print_r($checkRefferalCode);die;
                        if (empty($checkRefferalCode)) {
                            $data = array('ref_code' => $ref_code);
                            if ($isActive != "") {
                                $date['is_active'] = $isActive;
                            }
                            $addRefferalCode = $this->user_service->addRefferalCode($data);
                            if ($addRefferalCode) {
                                $this->response(array("message" => MESSAGE_conf::REFERRALCODE_ADDED), REST_Controller::HTTP_OK);
                            } else {
                                $this->response(array("message" => MESSAGE_conf::REFERRALCODE_ADDED_FAILED), REST_Controller::HTTP_BAD_REQUEST);
                            }
                        } else {
                            if ($checkRefferalCode['is_active'] != 1) {
                                $ref_code_id = $checkRefferalCode['referral_id'];
                                $data['is_active'] = 1;
                                $data['updated_at'] = date('Y-m-d H:i:s');
                                $updateRefferalCode = $this->user_service->updateRefferalCode($ref_code_id, $data);
                                if ($updateRefferalCode) {
                                    $this->response(array("message" => MESSAGE_conf::UPDATE_REFERRALCODE), REST_Controller::HTTP_OK);
                                } else {
                                    $this->response(array("message" => MESSAGE_conf::REFERRALCODE_UPDATE_FAILED), REST_Controller::HTTP_BAD_REQUEST);
                                }
                            } else {
                                $this->response(array("message" => MESSAGE_conf::ALREADY_HAVE), REST_Controller::HTTP_BAD_REQUEST);
                            }
                        }
                    } else {
                        $this->response(array("message" => MESSAGE_conf::ALL_REQUIRED), REST_Controller::HTTP_BAD_REQUEST);
                    }
                }
            }
            else 
            {
                $this->response(array("message" => MESSAGE_conf::UNAUTH), REST_Controller::HTTP_UNAUTHORIZED);

            }
           
        }
        else {
            return $result;
        }
    }

    /**********
     * Use:get all main categories
     * Method:get
     * Response : OK********* */
    public function getReferralCode_get($refId = "")
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);

        if (isset($result)) {
            if ($refId == "") {
                $getReferralCode = $this->user_service->getReferralCode();
            } else {
                $getReferralCode = $this->user_service->getReferralCodeById($refId);
            }

            if ($getReferralCode) {
                $this->response(array("message" => MESSAGE_conf::SUCCESS, 'data' => $getReferralCode), REST_Controller::HTTP_OK);
            } else {
                $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_OK);
            }
        } else {
            $result;
        }
    }
}
