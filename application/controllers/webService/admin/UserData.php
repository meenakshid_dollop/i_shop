<?php
require APPPATH . 'libraries/REST_Controller.php';
defined('BASEPATH') or exit('No direct script access allowed');
/**
 *@Author:Meenakshi
 *This controller working for user opration
 */
class UserData extends REST_Controller
{
    function __construct()

    {
        parent::__construct();

        $this->config->load('myConstant');
        $this->load->helper(array('form', 'url', 'Validation_helper'));
        $this->load->library('form_validation');
        $this->load->database('');
        $this->load->service('User_service');
        $this->load->library('Authorization_Token');
        $this->load->helper('date');
        date_default_timezone_set("Asia/Kolkata");
    }
    /************
     * Use:get all user buy admin
     * Method:Get
     * Response:OK
     * ************** */
    public function getUserByAdmin_get($userId="")
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if (isset($result)) 
        {
            $role = $result['role'];
            if ($role == 'Admin')
             {
                if($userId !="")
                {
                    $getUser = $this->user_service->getSingleUserByAdmin($userId);
                }
                else {
                    $getUser = $this->user_service->getUser();
                }
                if ($getUser) 
                {
                    $this->response(array("message" => MESSAGE_conf::SUCCESS,"data"=> $getUser), REST_Controller::HTTP_OK);
                }
                else 
                {
                    $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_OK);
                }
             }
             else {
                $this->response(array("message" => MESSAGE_conf::UNAUTH), REST_Controller::HTTP_BAD_REQUEST);

             }
        }
        else {
           return $result;
        }
    }




    /*******
     * Use:verfied user for the app
     * Method:Put
     * param:is_Active,is_Delete,is_verified
     * Response:OK
     * **************** */

     public function updateUserByAdmin_put($userId)
     {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if (isset($result)) {
            $role = $result['role'];
            if ($role == 'Admin') {
                $is_active = $this->put("is_active");
                $is_delete = $this->put("is_delete");
                $admin_verfiy = $this->put("admin_verfiy");
                if (isset($is_active) && $is_active != "") {
                    $updateData['is_active'] = $is_active;
                }
                if (isset($is_delete) && $is_delete != "") {
                    $updateData['is_delete'] = $is_delete;
                }
                if (isset($admin_verfiy) && $admin_verfiy != "") {
                    $updateData['admin_verfiy'] = $admin_verfiy;
                }
                $updateUserById = $this->user_service->updateUserById($userId, $updateData);
                if ($updateUserById) {
                    $this->response(array("message" => MESSAGE_conf::SUCCESS), REST_Controller::HTTP_OK);
                } else {
                    $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_BAD_REQUEST);
                }
            } else {
                $this->response(array("message" => MESSAGE_conf::UNAUTH), REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            return $result;
        }
     }

}
