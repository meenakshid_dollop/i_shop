<?php
require APPPATH . 'libraries/REST_Controller.php';
defined('BASEPATH') or exit('No direct script access allowed');
// @ use for the admin for category functionality
/////@ Author: Meenakshi
class CategoryController extends REST_Controller
{
    function __construct()

    {
        parent::__construct();

        $this->config->load('myConstant');
        $this->load->helper(array('form', 'url', 'Validation_helper'));
        $this->load->library('form_validation');
        $this->load->database('');
        $this->load->service('User_service');
        $this->load->library('Authorization_Token');
        $this->load->helper('date');
        date_default_timezone_set("Asia/Kolkata");
    }
    /*****
     * Use:add Category 
     * Method:post
     * Param : category Name Type : String
     *         isActive Type Tinyint = 0/1
     * Response : OK****** */
    public function Categories_post()
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        // print_r($_POST);
     
        if (isset($result)) {
            $categoryName = $this->input->post('categoryName');
            $isActive = $this->input->post('isActive');
            $isDelete = $this->input->post('isDelete');
            $categoryId = $this->input->post('category_id');
            if ($categoryId != "") {
                $data =  array();
                if ($categoryName != "") {
                    $data['category_name'] = $categoryName;
                }
                if ($isActive != "") {
                    $data['is_active'] = $isActive;
                }
                if ($isDelete != "") {
                    $data['is_delete'] = $isDelete;
                }
                if ((isset($_FILES['category_image']) && $_FILES['category_image']['name'] != "")) {
                    $image_data = $_FILES['category_image'];
                    $path = "upload/category/";
                    $image = $this->Common_model->upload_image($image_data, 1, $path);
                    $data['category_image'] = $path . $image;
                }
               // print_r();die;
                $updateCategory = $this->user_service->updateCategory($categoryId, $data);
                if ($updateCategory) {
                    $this->response(array("message" => MESSAGE_conf::UPDATE_CATEGORY), REST_Controller::HTTP_OK);
                } else {
                    $this->response(array("message" => MESSAGE_conf::CATEGORY_UPDATE_FAILED), REST_Controller::HTTP_BAD_REQUEST);
                }
            } else {
                if ($categoryName != "") {
                    $checkName = $this->user_service->checkNameCategories($categoryName);
                    // print_r($checkName);die;
                    if (empty($checkName)) {
                        $data = array('category_name' => $categoryName);
                        if ((isset($_FILES['category_image']) && $_FILES['category_image']['name'] != "")) {
                            $image_data = $_FILES['category_image'];
                            $path = "upload/category/";
                            $image = $this->Common_model->upload_image($image_data, 1, $path);
                            $data['category_image'] = $path.$image;
                        }
                        if ($isActive != "") {
                            $data['is_active'] = $isActive;
                        }
                        $addCategories = $this->user_service->addCategories($data);
                        if ($addCategories) {
                            $this->response(array("message" => MESSAGE_conf::CATEGORY_ADDED), REST_Controller::HTTP_OK);
                        } else {
                            $this->response(array("message" => MESSAGE_conf::CATEGORY_ADDED_FAILED), REST_Controller::HTTP_BAD_REQUEST);
                        }
                    } else {
                        if ($checkName['is_active'] != 1) {
                            $categoryId = $checkName['category_id'];
                            $data['is_active'] = 1;
                            $data['category_name'] = $categoryName;
                            if ((isset($_FILES['category_image']) && $_FILES['category_image']['name'] != "")) {
                                $image_data = $_FILES['category_image'];
                                $path = "upload/category/";
                                $image = $this->Common_model->upload_image($image_data, 1, $path);
                                $data['category_image'] = $path . $image;
                            }
                            $data['updated_at'] = date('Y-m-d H:i:s');
                            $updateCategory = $this->user_service->updateCategory($categoryId, $data);
                            if ($updateCategory) {
                                $this->response(array("message" => MESSAGE_conf::UPDATE_CATEGORY), REST_Controller::HTTP_OK);
                            } else {
                                $this->response(array("message" => MESSAGE_conf::CATEGORY_UPDATE_FAILED), REST_Controller::HTTP_BAD_REQUEST);
                            }
                        } else {
                            $this->response(array("message" => MESSAGE_conf::ALREADY_HAVE), REST_Controller::HTTP_BAD_REQUEST);
                        }
                    }
                } else {
                    $this->response(array("message" => MESSAGE_conf::ALL_REQUIRED), REST_Controller::HTTP_BAD_REQUEST);
                }
            }
        }
        else {
            return $result;
        }
    }

    /**********
     * Use:get all main categories
     * Method:get
     * Response : OK********* */
    public function getCategories_get($catId = "")
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);

        if (isset($result)) {
            if ($catId == "") {
                $getCategories = $this->user_service->getCategories();
            } else {
                $getCategories = $this->user_service->getCategorieyById($catId);
            }

            if ($getCategories) {
                $this->response(array("message" => MESSAGE_conf::SUCCESS, 'data' => $getCategories), REST_Controller::HTTP_OK);
            } else {
                $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_OK);
            }
        } else {
            $result;
        }
    }
}
