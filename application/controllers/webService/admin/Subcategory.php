<?php
require APPPATH . 'libraries/REST_Controller.php';
defined('BASEPATH') or exit('No direct script access allowed');
// @ use for the admin for subcategory functionality
/////@ Author: Meenakshi
class SubCategory extends REST_Controller
{
    function __construct()

    {
        parent::__construct();

        $this->config->load('myConstant');
        $this->load->helper(array('form', 'url', 'Validation_helper'));
        $this->load->library('form_validation');
        $this->load->database('');
        $this->load->service('User_service');
        $this->load->library('Authorization_Token');
        $this->load->helper('date');
        date_default_timezone_set("Asia/Kolkata");
    }
    /*****
     * Use:add Category 
     * Method:post
     * Param : category Name Type : String
     *         isActive Type Tinyint = 0/1
     * Response : OK****** */
    public function subCategories_post()
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if (isset($result)) {
            $subcategoryName = $this->input->post('sub_category_name');
            $isActive = $this->input->post('isActive');
            $isDelete = $this->input->post('isDelete');
            $MaincategoryId = $this->input->post('category_id');
            $subcategoryId = $this->input->post('sub_category_id');
            if ($subcategoryId != "") {
                if ($subcategoryName != "") {
                    $data['sub_category_name'] = $subcategoryName;
                }
                if ($MaincategoryId != "") {
                    $data['category_id'] = $MaincategoryId;
                }
                if ($isActive != "") {
                    $data['is_active'] = $isActive;
                }
                if ($isDelete != "") {
                    $data['is_delete'] = $isDelete;
                }
                if ((isset($_FILES['sub_category_image']) && $_FILES['sub_category_image']['name'] != "")) {
                    $image_data = $_FILES['sub_category_image'];
                    $path = "upload/subcategory/";
                    $image = $this->Common_model->upload_image($image_data, 1, $path);
                    $data['sub_category_image'] = $path . $image;
                }
                $updateCategory = $this->user_service->updateSubCategory($subcategoryId, $data);
                if ($updateCategory) {
                    $this->response(array("message" => MESSAGE_conf::UPDATE_CATEGORY), REST_Controller::HTTP_OK);
                } else {
                    $this->response(array("message" => MESSAGE_conf::CATEGORY_UPDATE_FAILED), REST_Controller::HTTP_BAD_REQUEST);
                }
            } else {
                if ($subcategoryName != "" && $MaincategoryId != "") {
                    $checkName = $this->user_service->checkNameSubCategories($subcategoryName, $MaincategoryId);
                    // print_r($checkName);die;
                    if (empty($checkName)) {
                        $data = array('sub_category_name' => $subcategoryName, 'category_id' => $MaincategoryId);
                        if ($isActive != "") {
                            $date['is_active'] = $isActive;
                        }
                        if ((isset($_FILES['sub_category_image']) && $_FILES['sub_category_image']['name'] != "")) {
                            $image_data = $_FILES['sub_category_image'];
                            $path = "upload/subcategory/";
                            $image = $this->Common_model->upload_image($image_data, 1, $path);
                            $data['sub_category_image'] = $path . $image;
                        }
                        $addSubCategories = $this->user_service->addSubCategories($data);
                        if ($addSubCategories) {
                            $this->response(array("message" => MESSAGE_conf::SUB_CATEGORY_ADDED), REST_Controller::HTTP_OK);
                        } else {
                            $this->response(array("message" => MESSAGE_conf::SUB_CATEGORY_ADDED_FAILED), REST_Controller::HTTP_BAD_REQUEST);
                        }
                    } else {
                        // print_r($checkName);die;
                        if ($checkName['is_active'] != 1) {
                            $subcategoryId = $checkName['sub_category_id'];
                            $data['is_active'] = 1;
                            $data['sub_category_name'] = $subcategoryName;
                            if ((isset($_FILES['sub_category_image']) && $_FILES['sub_category_image']['name'] != "")) {
                                $image_data = $_FILES['sub_category_image'];
                                $path = "upload/subcategory/";
                                $image = $this->Common_model->upload_image($image_data, 1, $path);
                                $data['sub_category_image'] = $path . $image;
                            }
                            $data['updated_at'] = date('Y-m-d H:i:s');
                            $updateCategory = $this->user_service->updateSubCategory($subcategoryId, $data);
                            if ($updateCategory) {
                                $this->response(array("message" => MESSAGE_conf::UPDATE_SUB_CATEGORY), REST_Controller::HTTP_OK);
                            } else {
                                $this->response(array("message" => MESSAGE_conf::SUB_CATEGORY_UPDATE_FAILED), REST_Controller::HTTP_BAD_REQUEST);
                            }
                        } else {
                            $this->response(array("message" => MESSAGE_conf::ALREADY_HAVE), REST_Controller::HTTP_BAD_REQUEST);
                        }
                    }
                } else {
                    $this->response(array("message" => MESSAGE_conf::ALL_REQUIRED), REST_Controller::HTTP_BAD_REQUEST);
                }
            }
        } else {
            return $result;
        }
    }

    /**********
     * Use:get all main categories
     * Method:get
     * Response : OK********* */
    public function getSubCategories_get($suCatId = "")
    {
        if ($suCatId == "") {
            $getSubCategories = $this->user_service->getSubCategories();
        } else {
            $getSubCategories = $this->user_service->getSubCategorieyById($suCatId);
        }

        if ($getSubCategories) {
            $this->response(array("message" => MESSAGE_conf::SUCCESS, 'data' => $getSubCategories), REST_Controller::HTTP_OK);
        } else {
            $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_OK);
        }
    }
}
