<?php
require APPPATH . 'libraries/REST_Controller.php';
defined('BASEPATH') or exit('No direct script access allowed');
// @ use for the admin for coupon functionality
/////@ Author: Meenakshi
class CouponController extends REST_Controller
{
    function __construct()

    {
        parent::__construct();

        $this->config->load('myConstant');
        $this->load->helper(array('form', 'url', 'Validation_helper'));
        $this->load->library('form_validation');
        $this->load->database('');
        $this->load->service('User_service');
        $this->load->library('Authorization_Token');
        $this->load->helper('date');
        date_default_timezone_set("Asia/Kolkata");
    }
    /*
	@Use :  Add Offer
    @Method : Post
    @Param: couponCode: Type : String
            couponNote Type : String
            discription Type : String
            percent Type :integer
            typeId Type :integer
            minimumAmount Type : integer
            uptoAmount Type : integer
            startDate Type :integer
            endDate  Type :integer
            couponImage Type :integer
	@Response : Ok
       */
    public function addCoupon_post()
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if (isset($result)) {
            $role = $result['role'];
            if ($role == 'Admin') 
            {
                $coupon_title = $this->input->post('coupon_title');
                $coupon_code = $this->input->post('coupon_code');
                $coupon_note = $this->input->post('coupon_note');
                $coupon_descrption = $this->input->post('coupon_descrption');
                $discount_percent = $this->input->post('discount_percent');
                $discount_amount = $this->input->post('discount_amount');
                $type = $this->input->post('type');
                $min_amount = $this->input->post('min_amount');
                $max_amount = $this->input->post('max_amount');
                $start_date = $this->input->post('start_date');
                $end_date = $this->input->post('end_date');
                $is_active = $this->input->post('is_active');
                $is_delete = $this->input->post('is_delete');                
                $coupon_id = $this->input->post('coupon_id');     
                if(isset($coupon_id) && $coupon_id !="")   
                {
                    if (isset($coupon_note) && $coupon_note != "") {
                        $updateCoupon['coupon_note'] = $coupon_note;
                    }
                    if (isset($coupon_title) && $coupon_title != "") {
                        $updateCoupon['coupon_title'] = $coupon_title;
                    }
                    if (isset($coupon_descrption) && $coupon_descrption != "") {
                        $updateCoupon['coupon_descrption'] = $coupon_descrption;
                    }
                    if (isset($discount_percent) && $discount_percent != "") {
                        $updateCoupon['discount_percent'] = $discount_percent;
                    }
                    if (isset($discount_amount) && $discount_amount != "") {
                        $updateCoupon['discount_amount'] = $discount_amount;
                    }
                    if (isset($type) && $type != "") {
                        $updateCoupon['user_type'] = $type;
                    }
                    if (isset($min_amount) && $min_amount != "") {
                        $updateCoupon['min_amount'] = $min_amount;
                    }
                    if (isset($is_active) && $is_active != "") {
                        $updateCoupon['is_active'] = $is_active;
                    }
                    if (isset($is_delete) && $is_delete != "") {
                        $updateCoupon['is_delete'] = $is_delete;
                    }
                    if (isset($max_amount) && $max_amount != "") {
                        $updateCoupon['max_amount'] = $max_amount;
                    }
                    if (isset($_FILES['coupon_image']) && $_FILES['coupon_image']['name'] != "") {
                    $image_data =  $_FILES['coupon_image'];
                        $this->load->library('upload');
                        $num = 1;
                        $path1 = "upload/coupon/";
                        $image = $this->Common_model->upload_image($image_data, $num, $path1);
                        $updateCoupon['coupon_image'] = $path1.$image;
                    }
                    $checkCouponCode = $this->user_service->CheckcouponCode($coupon_code);
                    if (empty($checkCouponCode)) {
                        $result =  $this->user_service->updateCoupon($coupon_id, $updateCoupon);                         
                    }
                    else 
                     {
                        if ($checkCouponCode['is_active'] != 1) {
                            $updateCoupon['is_active'] = 1;
                            $result =  $this->user_service->updateCoupon($coupon_id, $updateCoupon);                            
                        } else {
                            $this->response(array("message" => MESSAGE_conf::ALREADY_HAVE), REST_Controller::HTTP_UNAUTHORIZED);
                        }
                    }
                    if ($result) {
                        $this->response(array("message" => MESSAGE_conf::COUPON_UDATE), REST_Controller::HTTP_OK);
                    } else {
                        $this->response(array("message" => MESSAGE_conf::COUPON_NOT_UPDATE), REST_Controller::HTTP_BAD_REQUEST);
                    }
                } 
                else {
                    if (($discount_percent != "" || $discount_amount != "") && $coupon_code != "" && $coupon_note != "" && $coupon_descrption != "" && $type != "" && $min_amount != "" && $max_amount != "" && $start_date != "" && $end_date != "") {
                        $checkCouponCode = $this->user_service->CheckcouponCode($coupon_code);
                        if (empty($checkCouponCode)) {
                            $couponDetails = array(
                                'coupon_code' => $coupon_code,
                                'coupon_note' => $coupon_note,
                                'coupon_title' => $coupon_title,
                                'coupon_descrption' => $coupon_descrption,
                                'user_type' => $type,
                                'discount_percent' => $discount_percent,
                                'discount_amount' => $discount_amount,
                                'min_amount' => $min_amount,
                                'max_amount' => $max_amount,
                                'start_date' =>  date("Y-m-d H:i:s", strtotime($start_date)),
                                'end_date' => date("Y-m-d H:i:s", strtotime($end_date))
                            );
                            if (isset($_FILES['coupon_image']) && $_FILES['coupon_image']['name'] != "") {
                                $image_data =  $_FILES['coupon_image'];
                                $this->load->library('upload');
                                $num = 1;
                                $path1 = "upload/coupon/";
                                $image = $this->Common_model->upload_image($image_data, $num, $path1);
                                $couponDetails['coupon_image'] = $path1.$image;
                            }
                            $result =  $this->user_service->addCoupon($couponDetails);

                            if ($result) {
                                $this->response(array("message" => MESSAGE_conf::COUPON_ADDED), REST_Controller::HTTP_OK);
                            } else {
                                $this->response(array("message" => MESSAGE_conf::COUPON_NOT_ADDED), REST_Controller::HTTP_BAD_REQUEST);
                            }
                        } else {
                            if ($checkCouponCode['is_active'] != 1) {
                                $coupon_id = $checkCouponCode['coupon_id'];
                                $updateCoupon['is_active'] = 1;
                                if (isset($coupon_note) && $coupon_note != "") {
                                    $updateCoupon['coupon_note'] = $coupon_note;
                                }
                                if (isset($coupon_title) && $coupon_title != "") {
                                    $updateCoupon['coupon_title'] = $coupon_title;
                                }
                                if (isset($coupon_descrption) && $coupon_descrption != "") {
                                    $updateCoupon['coupon_descrption'] = $coupon_descrption;
                                }
                                if (isset($discount_percent) && $discount_percent != "") {
                                    $updateCoupon['discount_percent'] = $discount_percent;
                                }
                                if (isset($discount_amount) && $discount_amount != "") {
                                    $updateCoupon['discount_amount'] = $discount_amount;
                                }
                                if (isset($type) && $type != "") {
                                    $updateCoupon['user_type'] = $type;
                                }
                                if (isset($min_amount) && $min_amount != "") {
                                    $updateCoupon['min_amount'] = $min_amount;
                                }
                                if (isset($max_amount) && $max_amount != "") {
                                    $updateCoupon['max_amount'] = $max_amount;
                                }
                                if (isset($_FILES['coupon_image']) && $_FILES['coupon_image']['name'] != "") {
                                    $image_data =  $_FILES['coupon_image'];
                                    $this->load->library('upload');
                                    $num = 1;
                                    $path1 = "upload/coupon/";
                                    $image = $this->Common_model->upload_image($image_data, $num, $path1);
                                    $updateCoupon['coupon_image'] = $path1.$image;
                                }
                                $result =  $this->user_service->updateCoupon($coupon_id, $updateCoupon);
                                if ($result) {
                                    $this->response(array("message" => MESSAGE_conf::COUPON_UDATE), REST_Controller::HTTP_OK);
                                } else {
                                    $this->response(array("message" => MESSAGE_conf::COUPON_NOT_UPDATE), REST_Controller::HTTP_BAD_REQUEST);
                                }
                            } else {
                                $this->response(array("message" => MESSAGE_conf::ALREADY_HAVE), REST_Controller::HTTP_UNAUTHORIZED);
                            }
                        }
                    } else {
                        $this->response(array("message" => MESSAGE_conf::ALL_REQUIRED), REST_Controller::HTTP_BAD_REQUEST);
                    }
                }        
                
            } else {
                $this->response(array("message" => MESSAGE_conf::UNAUTH), REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            return $result;
        }
    }




    /**********
     * Use:get all main categories
     * Method:get
     * Response : OK********* */
    public function getCoupon_get($couponId = "")
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if (isset($result)) {
            if ($couponId == "") {
                $getCoupon = $this->user_service->getCoupon();
            } else {
                $getCoupon = $this->user_service->getCouponById($couponId);
            }

            if ($getCoupon) {
                $this->response(array("message" => MESSAGE_conf::SUCCESS, 'data' => $getCoupon), REST_Controller::HTTP_OK);
            } else {
                $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_OK);
            }
        } else {
            $result;
        }
    }
}
