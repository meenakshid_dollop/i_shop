<?php
require APPPATH . 'libraries/REST_Controller.php';
/**
 *@Author:Meenakshi
 *This controller working for commom  opration
 */
class CommonController extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->config->load('myConstant');
        $this->load->library('session');
        $this->load->library('Authorization_Token');
        $this->load->helper(array('form', 'url', 'Validation_helper'));
        $this->load->library('form_validation');
        $this->load->database('');
        $this->load->service('User_service');
    }

    /* 
    Use:For Change Password of the admin
    Method:Put
    Param  :Old Password(String)
            new Password(String)
    Response :OK
    */
   public function change_password_put()
   {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if($result)
        {
            $role = $result['role'];
            if($role == "Admin"){
                $old_password  =  $this->put('old_password');
                $new_password  =  $this->put('new_password');
                if (isset($old_password) && $old_password != "" && isset($new_password) && $new_password != "") {
                    $checkPassword  = $this->user_service->checkPassword($old_password);
                    if ($checkPassword) {
                        $changePassword = $this->user_service->changePassword($new_password);
                        if ($changePassword) {
                            $this->response(array("message" => MESSAGE_conf::SUCCESS), REST_Controller::HTTP_OK);
                        } else {
                            $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_BAD_REQUEST);
                        }
                    } else {
                        $this->response(array("message" => MESSAGE_conf::OLD_PASSWORD_NOT_MATCH), REST_Controller::HTTP_BAD_REQUEST);
                    }
                } else {
                    $this->response(array("message" => MESSAGE_conf::ALL_REQUIRED), REST_Controller::HTTP_BAD_REQUEST);
                }
            }
            else 
            {
                $this->response(array("message" => MESSAGE_conf::UNAUTH), REST_Controller::HTTP_BAD_REQUEST);
            }  
        }
        else 
        {
            return $result;
        }
   }



    



   

}
