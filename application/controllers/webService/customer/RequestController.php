<?php
require APPPATH . 'libraries/REST_Controller.php';
/**
 *@Author:Meenakshi
 *This controller working for request  opration
 */
class RequestController extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->config->load('myConstant');
        $this->load->library('session');
        $this->load->library('Authorization_Token');
        $this->load->helper(array('form', 'url', 'Validation_helper'));
        $this->load->library('form_validation');
        $this->load->database('');
        $this->load->service('User_service');
    }
    /******
     * Use:add product or request and edit
     * Method:post
     * Param:   category_id(int)
            * sub_category_id(int)
            * category_id(int)
            * request_title(string)
            * request_sub_title(string)
            * request_amount(int)
            * request_condition(string)
            * request_type(string)
            * lat(int)
            * long(int)
    *Response:OK
     **** */
    public function addRequest_post()
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if (isset($result)) {
            $role = $result['role'];
            if($role == "User")
            {             
                $id = $result['id'];      
                $category_id      =   $this->input->post('category_id');
                $sub_category_id   =$this->input->post('sub_category_id');
                $request_title = $this->input->post('request_title');
                $request_sub_title = $this->input->post('request_sub_title');
                $request_amount   =$this->input->post('request_amount');
                $request_condition = $this->input->post('request_condition');
                $request_type = $this->input->post('request_type');
                $request_id = $this->input->post('request_id');
                $lat = $this->input->post('lat');
                $long = $this->input->post('long');
                if(isset($request_id) && $request_id!="")
                {
                    $updateRequestData = array('updated_at'=>date('Y-m-d H:i:s'));
                    if(isset($category_id) && $category_id !="")
                    {
                       $updateRequestData['category_id'] = $category_id;
                    }
                    if(isset($sub_category_id) && $sub_category_id !="")
                    {
                       $updateRequestData['sub_category_id'] = $sub_category_id;
                    }
                    if(isset($request_title) && $request_title !="")
                    {
                       $updateRequestData['request_title'] = $request_title;
                    }
                    if(isset($request_sub_title) && $request_sub_title !="")
                    {
                       $updateRequestData['request_sub_title'] = $request_sub_title;
                    }
                    if(isset($request_condition) && $request_condition !="")
                    {
                       $updateRequestData['request_condition'] = $request_condition;
                    }
                    if(isset($long) && $long !="")
                    {
                       $updateRequestData['long'] = $long;
                    }
                    if(isset($long) && $long !="")
                    {
                       $updateRequestData['long'] = $long;
                    }                  
                    $updateRequest = $this->user_service->updateRequest($request_id,$updateRequestData);
                    if($updateRequest)
                    {
                        $this->response(array("message" => MESSAGE_conf::UPDATE_PRODUCT), REST_Controller::HTTP_OK);
                    }
                    else {
                        $this->response(array("message" => MESSAGE_conf::PRODUCT_UPDATE_FAILED), REST_Controller::HTTP_BAD_REQUEST);
                    }
                }
                else 
                { 
                    if ($lat != "" && $long != "" && $request_title != "" &&  $request_amount != "" && $request_condition != "") {
                     
                        $requestData  = array(
                            'request_title' => $request_title,
                            'request_amount' => $request_amount,
                            'request_condition' => $request_condition,
                            'request_type' => $request_type,
                            'user_id' => $id,
                            'lat' => $lat,
                            'long' => $long,
                        );
                        if ((isset($_FILES['request_image']) && $_FILES['request_image']['name'] != "")) {
                            $image_data = $_FILES['request_image'];
                            $path = "upload/product/";
                            $image = $this->Common_model->upload_image($image_data, 1, $path);
                            $requestData['request_image'] = $path . $image;
                        }
                      
                            
                            // if (isset($_FILES['request_image']) && $_FILES['request_image']!= "") {
                            // $customImage = $this->user_service->upload_multiple_images($_FILES['request_image']);
                            // $customImage .= "," . $hidden_image;
                            // $customImage = trim($customImage, ",");
                            // $customImage = trim($customImage, " ");
                            // $requestData['request_image'] = $customImage;
                            // }

                            
                        if($request_type == "product")
                         {  
                            if ($sub_category_id != "" && $category_id != "") {
                                $requestData['sub_category_id'] = $sub_category_id;
                                $requestData['category_id'] = $category_id;
                                $addRequest = $this->user_service->addRequest($requestData);
                            } else {
                                $this->response(array("message" => MESSAGE_conf::ALL_REQUIRED), REST_Controller::HTTP_BAD_REQUEST);
                            }
                        }
                        if ($request_type == "service") {
                            
                            $addRequest = $this->user_service->addRequest($requestData);
                        }
                        
                        if ($addRequest) {
                            $this->response(array("message" => MESSAGE_conf::PRODUCT_ADDED), REST_Controller::HTTP_OK);
                        } else {
                            $this->response(array("message" => MESSAGE_conf::PRODUCT_ADDED_FAILED), REST_Controller::HTTP_OK);
                        }
                    } else {
                        $this->response(array("message" => MESSAGE_conf::ALL_REQUIRED), REST_Controller::HTTP_BAD_REQUEST);
                    }
                }                
            }
            else 
            {
                $this->response(array("message" => MESSAGE_conf::UNAUTH), REST_Controller::HTTP_BAD_REQUEST);
            }
        }
        else {
            return $result;
        }
    }

   /*******
    * Use:Get all request 
    * Method:Get
    * Response:OK
    ************* */

    public function getAllRequest_get($request_id = "")
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if (isset($result)) 
        {            
            if($request_id !="")
            {
                $ProductArray = "";
                $getRequest = $this->user_service->getRequestById($request_id);
                $requestArray  = "";
                $getRequest['created_at'] =  time_Ago($getRequest['created_at']);               
                if ($getRequest['request_type'] == "service") {
                    $requestArray  = $getRequest;
                }
                if ($getRequest['request_type'] == "product") {
                    $requestArray  = $getRequest;
                }
            }
            else 
            {
                $requestArray = array();
                $ProductArray = array();
                $getRequest = $this->user_service->getAllRequest();
                foreach ($getRequest as $value) {
                    $createdDate = $value['created_at'];
                    $value['created_at'] =  time_Ago($createdDate);
                    if($value['request_type'] == "service")
                    {
                        array_push($requestArray, $value);
                    }
                    if($value['request_type'] == "product")
                    {
                        array_push($ProductArray, $value);
                    }
                }                
            }           
            if($getRequest)
            {                    
                $this->response(array("message" => MESSAGE_conf::SUCCESS,"request"=> $requestArray,"product"=> $ProductArray), REST_Controller::HTTP_BAD_REQUEST);
            }
            else 
            {
                $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_BAD_REQUEST);
            }
        }
        else 
        {
            return $result;
        }

    }
}
