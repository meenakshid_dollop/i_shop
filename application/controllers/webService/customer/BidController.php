<?php
require APPPATH . 'libraries/REST_Controller.php';
/**
 *@Author:Meenakshi
 *This controller working for request  opration
 */
class BidController extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->config->load('myConstant');
        $this->load->library('session');
        $this->load->library('Authorization_Token');
        $this->load->helper(array('form', 'url', 'Validation_helper'));
        $this->load->library('form_validation');
        $this->load->database('');
        $this->load->service('User_service');
    }

    /**********
     * Use:Place bid 
     * Method:post
     * Param:request_id(int)
     *        bid_description(string)  
     *        bid_amount(int)  
     * Response:OK
     * ************* */
    public function placeBid_post()
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if ($result) {
            $id = $result['id'];
            $request_id = $this->input->post("request_id");
            $bid_id = $this->input->post("bid_id");
            $bid_description = $this->input->post("bid_description");
            $bid_amount = $this->input->post("bid_amount");
            $checkBid = $this->user_service->checkBid($request_id, $id);         
            if ($bid_id != "" || $checkBid) {
                if(isset($checkBid))
                {
                    $bid_id = $checkBid['bid_id'];
                }
                $updateData = array('updated_at' => date('Y-m-d H:i:s'));
                if ($bid_description != "" && isset($bid_description)) {
                   
                    $updateData['bid_description']  = $bid_description;
                }
                if ($bid_amount != "" && isset($bid_amount)) {
                    $updateData['bid_amount']  = $bid_amount;
                }
                if (isset($_FILES['bid_image']) && $_FILES['bid_image']['name'] != "") {
                    $image_data = $_FILES['bid_image'];
                    $path = "upload/bid/";
                    $image = $this->Common_model->upload_image($image_data, 1, $path);
                    $updateData['bid_image'] = $path . $image;
                }
                $updateBid = $this->user_service->updateBid($bid_id, $updateData);
                if ($updateBid) {
                    $this->response(array("message" => MESSAGE_conf::BID_UPDATE), REST_Controller::HTTP_OK);
                } else {
                    $this->response(array("message" => MESSAGE_conf::BID_NOT_UPDATE), REST_Controller::HTTP_BAD_REQUEST);
                }
            } else {
                if ($request_id != "" && $bid_description != "" && $bid_amount != "") {
                    if ($bid_description != "" && isset($bid_description)) {
                        $updateData['bid_description']  = $bid_description;
                    }
                    if ($bid_amount != "" && isset($bid_amount)) {
                        $updateData['bid_amount']  = $bid_amount;
                    }
                    if (isset($_FILES['bid_image']) && $_FILES['bid_image']['name'] != "") {
                        $image_data = $_FILES['bid_image'];
                        $path = "upload/bid/";
                        $image = $this->Common_model->upload_image($image_data, 1, $path);
                        $updateData['bid_image'] = $path . $image;
                    }
                    $updateBid = $this->user_service->updateBid($bid_id, $updateData);
                    if ($updateBid) {
                        $this->response(array("message" => MESSAGE_conf::BID_UPDATE), REST_Controller::HTTP_OK);
                    } else {
                        $this->response(array("message" => MESSAGE_conf::BID_NOT_UPDATE), REST_Controller::HTTP_BAD_REQUEST);
                    }

                    $data = array(
                        'request_id' => $request_id,
                        "bid_description" => $bid_description,
                        'bid_amount' => $bid_amount
                    );
                    if (isset($_FILES['bid_image']) && $_FILES['bid_image']['name'] != "") {
                        $image_data = $_FILES['bid_image'];
                        $path = "upload/bid/";
                        $image = $this->Common_model->upload_image($image_data, 1, $path);
                        $userData['bid_image'] = $path . $image;
                    }
                    $addBid = $this->user_service->addBid($data);
                    if ($addBid) {
                        $this->response(array("message" => MESSAGE_conf::BID_PLACED), REST_Controller::HTTP_OK);
                    } else {
                        $this->response(array("message" => MESSAGE_conf::BID_NOT_PLACED), REST_Controller::HTTP_BAD_REQUEST);
                    }
                } else {
                    $this->response(array("message" => MESSAGE_conf::ALL_REQUIRED), REST_Controller::HTTP_BAD_REQUEST);
                }
            }
        } else {
            return $result;
        }
    }

    /*********
     * Use:get bid
     * Method:Get
     * Param:
     * Response:OK
     * ************** */

    public function getAllBid_get($request_id)
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if ($result) {
            $bidAcceptArray = array();
            $bidCancelArray = array();
            $bidCompleteArray = array();
            $bidPendingArray = array();
           
            $getBid = $this->user_service->getAllBidByRequestId($request_id);
           // print_r($getBid);die;
            if ($getBid) {
                foreach ($getBid['accept'] as  $value) 
                {
                    $date = $value['created_at'];                    
                    $requestDate = $value['requestDate'];
                    $value['bidTime']=  time_Ago($date);
                    $value['requestTime'] =  time_Ago($requestDate);
                    $value['TotalBid'] = count($getBid);                   
                    array_push($bidAcceptArray, $value);                   
                    
                }
                foreach ($getBid['cancel'] as  $value) 
                {
                    $date = $value['created_at'];                    
                    $requestDate = $value['requestDate'];
                    $value['bidTime']=  time_Ago($date);
                    $value['requestTime'] =  time_Ago($requestDate);
                    $value['TotalBid'] = count($getBid);                   
                    array_push($bidCancelArray, $value);                   
                    
                }
                foreach ($getBid['complete'] as  $value) 
                {
                    $date = $value['created_at'];                    
                    $requestDate = $value['requestDate'];
                    $value['bidTime']=  time_Ago($date);
                    $value['requestTime'] =  time_Ago($requestDate);
                    $value['TotalBid'] = count($getBid);                   
                    array_push($bidCompleteArray, $value);                   
                    
                }
                foreach ($getBid['pending'] as  $value) 
                {
                    $date = $value['created_at'];                    
                    $requestDate = $value['requestDate'];
                    $value['bidTime']=  time_Ago($date);
                    $value['requestTime'] =  time_Ago($requestDate);
                    $value['TotalBid'] = count($getBid);                   
                    array_push($bidPendingArray, $value);                   
                    
                }
                $this->response(array("message" => MESSAGE_conf::SUCCESS, "accept" =>$bidAcceptArray, "pending" => $bidPendingArray, "complete" => $bidCompleteArray, "cancel" => $bidCancelArray), REST_Controller::HTTP_OK);
            } else {
                $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_OK);
            }
        } else {
            return $result;
        }
    }
    /*********
     * Use:get my bids
     * Method:Get
     * Param:user _id
     * Response:OK
     * ************** */
    public function getUserBid_get()
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if ($result) {
            $id = $result['id'];
            $bidArray = array();
            $bidAcceptedArray = array();
            $bidCompleteArray = array();
            $getBid = $this->user_service->getUserBid($id);        
            if ($getBid) {
                foreach ($getBid as  $value) {
                   
                    $date = $value['created_at'];
                    $requestDate = $value['requestDate'];
                    $value['bidTime'] =  time_Ago($date);
                    $value['requestTime'] =  time_Ago($requestDate);
                    $value['TotalBid'] = count($getBid);
                    if ($value['bid_status'] == "pending") {
                        array_push($bidArray, $value);
                    }
                    if ($value['bid_status'] == "accept") {
                        array_push($bidAcceptedArray, $value);
                    }
                    if ($value['bid_status'] == "complete") {
                        array_push($bidCompleteArray, $value);
                    }
                }
                $this->response(array("message" => MESSAGE_conf::SUCCESS, "pending" => $bidArray, "complete" => $bidCompleteArray, "accept" => $bidAcceptedArray), REST_Controller::HTTP_OK);
            } else {
                $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_OK);
            }
        } else {
            return $result;
        }
    }


    /***********
     * Use:Update bid status
     * Method:put
     * Param:bidID(int)
     *       status(String)
     * Response:OK
     * ************ */
    public function UpdateBidStatus_put()
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if ($result) 
        {
            $bidId = $this->put('bidId');
            $bidStatus = $this->put('bidStatus');
            $requirdData  = array('bidId' =>$bidId,
                                'bidStatus' => $bidStatus );
            if($bidId !="" && $bidStatus !="" )
            {
                $updateData =  array('updated_at' =>date('Y-m-d H:i:s') ,
                                    'bid_status'=> $bidStatus );
                $updateBid = $this->user_service->updateBid($bidId, $updateData);
                if ($updateBid) {
                    $this->response(array("message" => MESSAGE_conf::BID_UPDATE), REST_Controller::HTTP_OK);
                } else {
                    $this->response(array("message" => MESSAGE_conf::BID_NOT_UPDATE), REST_Controller::HTTP_BAD_REQUEST);
                }
            }
            else 
            {
                $this->response(array("message" => MESSAGE_conf::ALL_REQUIRED,"data"=> $requirdData), REST_Controller::HTTP_OK);

            }
        }
        else 
        {
            return $result;
        }
    }
}
