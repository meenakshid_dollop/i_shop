<?php
require APPPATH . 'libraries/REST_Controller.php';
/**
 *@Author:Meenakshi
 *This controller working for user opration
 */
class UserController extends REST_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->config->load('myConstant');
		$this->load->library('session');
		$this->load->library('Authorization_Token');
		$this->load->helper(array('form', 'url', 'Validation_helper'));
		$this->load->library('form_validation');
		$this->load->database('');
		$this->load->service('User_service');
	}

	/********
	 * Use: User registration and update profile
	 * Method: POST
	 * Param : 	name(string)
	 * 		  	email(string)
	 *       	lat(string)
	 * 			long(string)
	 * 			address(string)
				fcmid(string)
				deviceType(string)
	 * Response : OK
	 *  ******** */

	
	public function user_edit_profile_post()
	{
		$headers = $this->input->request_headers();
		$result = tokenVerification($headers);
		if ($result) 
		{
			$id = $result['id'];
			$name = $this->input->post("user_name")!=""?$this->input->post("user_name"):"";
			// $email = $this->input->post("user_email")!=""?$this->input->post("user_email"):"";
			$gender = $this->input->post("gender")!=""?$this->input->post("gender"):"";
			$lat = $this->input->post("lat")!=""?$this->input->post("lat"):"";
			$long = $this->input->post("long")!=""?$this->input->post("long"):"";
			//$password = $this->input->post("user_password")!=""?$this->input->post("user_password"):"";
			$device_type = $this->input->post("device_type")!=""?$this->input->post("device_type"):"";
			$user_address = $this->input->post("user_address")!=""?$this->input->post("user_address"):"";
			$fcm_id = $this->input->post("fcm_id")!=""?$this->input->post("fcm_id"):"";
			$userData = array(
				'user_name' => $name,
			//	'user_email' => $email,
				'user_address' => $user_address,
				'gender' => $gender,
				'device_type' => $device_type,
				'lat' => $lat,
				'long' => $long,
				'fcm_id' => $fcm_id,			
				
			);
			$getUserById = $this->user_service->getUserById($id);
			if($getUserById != 0)
			{
				if (isset($getUserById)  && $getUserById['is_register'] != 1) {
					if ($name != "" && $lat != "" && $long != "" && $user_address != "" && $gender != "" && $device_type != "" && $fcm_id != "") {
						if (isset($_FILES['government_id'])) {

							if (isset($_FILES['government_id']) && $_FILES['government_id']['name'] != "") {
								$image_data = $_FILES['government_id'];
								$path = "upload/user/";
								$image = $this->Common_model->upload_image($image_data, 1, $path);
								$userData['government_id'] = $path . $image;
							}
							if (isset($_FILES['user_profile']) && $_FILES['user_profile']['name'] != "") {
								$image_data = $_FILES['user_profile'];
								$path = "upload/user/";
								$image = $this->Common_model->upload_image($image_data, 1, $path);
								$userData['user_profile'] = $path . $image;
							}
							$getotp = rand(1111, 9999);
							$userData['otp'] = $getotp;
							$userData['is_register'] = 1;
							// $selfCode = getRandomTokenForUserReferal();
							// $userData['self_referral_code'] = $selfCode;
							
							//$userData['is_register'] = 1;
							$updateUserById = $this->user_service->updateUserById($id, $userData);
							if ($updateUserById) {
								$this->response(array("message" => MESSAGE_conf::SUCCESS), REST_Controller::HTTP_OK);
							} else {
								$this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_BAD_REQUEST);
							}
							//	}

						} else {
							$userData['government_id'] = "";
							$this->response(array("message" => MESSAGE_conf::ALL_REQUIRED, "data" => $userData), REST_Controller::HTTP_BAD_REQUEST);
						}
					} else {
						$this->response(array("message" => MESSAGE_conf::ALL_REQUIRED, "data" => $userData), REST_Controller::HTTP_BAD_REQUEST);
					}
				} else {
					$updateData = array('updated_at' => date('Y-m-d H:i:s'));
					if (isset($name) && $name != "") {
						$updateData['user_name'] = $name;
					}
					if (isset($gender) && $gender != "") {
						$updateData['gender'] = $gender;
					}
					if (isset($user_address) && $user_address != "") {
						$updateData['user_address'] = $user_address;
					}
					if (isset($_FILES['user_profile']) && $_FILES['user_profile']['name'] != "") {
						$image_data = $_FILES['user_profile'];
						$path = "upload/user/";
						$image = $this->Common_model->upload_image($image_data, 1, $path);
						$updateData['user_profile'] = $path . $image;
					}
					$updateUserById = $this->user_service->updateUserById($id, $updateData);
					if ($updateUserById) {
						$this->response(array("message" => MESSAGE_conf::SUCCESS), REST_Controller::HTTP_OK);
					} else {
						$this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_BAD_REQUEST);
					}
				}
			} else {
				$this->response(array("message" => MESSAGE_conf::NO_USER), REST_Controller::HTTP_BAD_REQUEST);
			}
						
		}
		else
		{
			return $result;
		}
		
	}



/* 
	Method : POST
	Use : For User Login
	Param : Email(String)
			Password (String)
	Response : OK
*/
	public function user_login_post()
	{
		$mobile = $this->input->post('mobile');		
		$device_type = $this->input->post('device_type');
		$fcm_id = $this->input->post('fcm_id');				
		if((isset($mobile) && $mobile !="") &&  (isset($device_type) && $device_type != "") && (isset($fcm_id) && $fcm_id != "") )
		{
			$userLogin = $this->user_service->checkMobile($mobile);
			if($userLogin)
			{
				if($userLogin['is_active'] == 1)
				{
					if($userLogin['is_delete'] == "not_deleted")
					{
						if ($userLogin['is_verify'] == 1) 
						{
							if($userLogin['admin_verfiy'] == 1)
							{
								$id = $userLogin['user_id'];
								$get_otp = rand(1111, 9999);
								//$text =-"Your otp is ". $otp;
								$userData = array(
									'otp' => $get_otp,
									'device_type' => $device_type,
									'fcm_id' => $fcm_id
								);
								$updateUserById = $this->user_service->updateUserById($id, $userData);
								// $sendOTp =  sendMessage($text, $mobileNo);
								if ($updateUserById) {
									$this->response(array("message" => MESSAGE_conf::SUCCESS, "OTP" => "$get_otp"), REST_Controller::HTTP_OK);
								} else {
									$this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_BAD_REQUEST);
								}

							} else {
								$this->response(array("message" => MESSAGE_conf::UNDER_VERIFICATION), REST_Controller::HTTP_BAD_REQUEST);
							}
								
						}
						else 
						{
							$this->response(array("message" => MESSAGE_conf::UNVERIFIED), REST_Controller::HTTP_BAD_REQUEST);	
						}
					}
					else 
					{
						$this->response(array("message" => MESSAGE_conf::DELETED_USER), REST_Controller::HTTP_BAD_REQUEST);
					}

				}
				else
				{
					$this->response(array("message" => MESSAGE_conf::DEACTIVEDTED_USER), REST_Controller::HTTP_BAD_REQUEST);
				}

			}
			else
			{
				$this->response(array("message" => MESSAGE_conf::INVALID_DETAILS), REST_Controller::HTTP_BAD_REQUEST);
			}
		}
		else
		{
			$this->response(array("message" => MESSAGE_conf::ALL_REQUIRED), REST_Controller::HTTP_BAD_REQUEST);
		}

	}

	/********
	 * Use: User Signup
	 * Method:POST
	 * Param : email(string)
	 * 		   Password (string)
	 * 		   	mobile ()
	 * Response : OK
	 *  ******** */

	 public function user_signup_post()
	 {
		$mobile = $this->input->post("mobile") != "" ? $this->input->post("mobile") : "";
		$referralCode = $this->input->post("referral_code") != "" ? $this->input->post("referral_code") : "";		
		$checkMobile = $this->user_service->checkMobile($mobile);	
		if ($checkMobile != 0) {
			if($checkMobile['is_verify'] != 0 )
			{
				$this->response(array("message" => MESSAGE_conf::MOBILE_EXIST), REST_Controller::HTTP_BAD_REQUEST);
			}
			else 
			{
				$id = $checkMobile['user_id'];
				$userData['referral_code'] = $referralCode;
				$getotp = rand(1111, 9999);
				$userData['otp'] = $getotp;
				$updateUserById = $this->user_service->updateUserById($id, $userData);
				if ($updateUserById) {
					$this->response(array("message" => MESSAGE_conf::SUCCESS, "otp" => $getotp), REST_Controller::HTTP_CREATED);
				} else {
					$this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_BAD_REQUEST);
				}
			}
		} else {
			if($mobile !="" && $referralCode !="")
			{
				$userData['mobile'] = $mobile;
				$userData['referral_code'] = $referralCode;
				$getotp = rand(1111, 9999);
				$userData['otp'] = $getotp;
				$createUser = $this->user_service->createUser($userData);
				if ($createUser) {
					$this->response(array("message" => MESSAGE_conf::SUCCESS, "otp" => $getotp), REST_Controller::HTTP_CREATED);
				} else {
					$this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_BAD_REQUEST);
				}
			}
			else 
			{
				$this->response(array("message" => MESSAGE_conf::ALL_REQUIRED), REST_Controller::HTTP_BAD_REQUEST);

			}
			
		}
		
	 }

	/* 
	Method :Post
	Use :Match Otp 
	Param :Otp(Integer),
		   Mobile No.(int)
	Responce :OK	 
	*/
	public function match_otp_post()
	{
		$mobile = $this->input->post("mobile");		
		$otp = $this->input->post("otp");		
		$result = $this->user_service->matchOtp($mobile, $otp);
		if ($result) 
		{
			$id = $result['user_id'];
			$setVerification = array(
					'is_verify' => 1,
					'updated_at' => date('Y-m-d H:i:s')
				);
			$result2 = $this->user_service->updateUserById($id, $setVerification);
			if ($result2) 
			{		
				$userData['mobile'] = $result['mobile'];
				$userData['role'] = "User";
				$userData['id'] = $id;		
				$tokenData = $this->authorization_token->generateToken($userData);
				$token = $tokenData;
				$result['token']=$token;
				$this->response(array("message" => MESSAGE_conf::SUCCESS, "data" => $result,), REST_Controller::HTTP_OK);
			}
			 else 
			 {
				$this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_BAD_REQUEST);
			 }
		} 
		else 
		{
			$this->response(array("message" => MESSAGE_conf::OTP_NOT_VERIFIED), REST_Controller::HTTP_BAD_REQUEST);
		}
	}

	/***
	 * Use :Resend Otp 
	 * Method : Put
	 * Param :mobile
	 * Response : OK
	 * *** */
	public function resend_otp_put()
	{
		$mobile_no = $this->put("mobile");			
		if (!empty($mobile_no)) 
		{
			$result = $this->user_service->checkMobile($mobile_no);		 
			if (!empty($result)) 
			{
				$id =  $result['user_id'];
				$getotp = rand(1111, 9999);
				$otp = array('otp' => $getotp);
				// print_r($getotp);
				//$text = "Your otp is " . $getotp;
				$RegenerateOtp = $this->user_service->updateUserById($id, $otp);
				//	$sendOTp =  sendMessage($text, $mobileNo);
				if ($RegenerateOtp) {
					$this->response(array("message" => MESSAGE_conf::SUCCESS, "OTP" => "$getotp"), REST_Controller::HTTP_CREATED);
				} else {
					$this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_BAD_REQUEST);
				}
			} 
			else 
			{
				$this->response(array("message" => MESSAGE_conf::NO_USER), REST_Controller::HTTP_UNAUTHORIZED);
			}
		}
		else 
		{
			$this->response(array("message" => MESSAGE_conf::ALL_REQUIRED), REST_Controller::HTTP_BAD_REQUEST);
		}
	}

	public function getUserProfile_get()
	{
		$headers = $this->input->request_headers();
		$result = tokenVerification($headers);
		if ($result) 
		{
			$id = $result['id'];
			$getUserProfile = $this->user_service->getUserProfile($id);
			if($getUserProfile)
			{
				$this->response(array("message" => MESSAGE_conf::SUCCESS,"data"=> $getUserProfile), REST_Controller::HTTP_OK);

			}
			else {
				$this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_OK);
			}
		}
		else {
			return $result;
		}
	}


}
