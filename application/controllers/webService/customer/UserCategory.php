<?php
require APPPATH . 'libraries/REST_Controller.php';
defined('BASEPATH') or exit('No direct script access allowed');
// @ use for the admin for subcategory functionality
/////@ Author: Meenakshi
class UserCategory extends REST_Controller
{
    function __construct()

    {
        parent::__construct();

        $this->config->load('myConstant');
        $this->load->helper(array('form', 'url', 'Validation_helper'));
        $this->load->library('form_validation');
        $this->load->database('');
        $this->load->service('User_service');
        $this->load->library('Authorization_Token');
        $this->load->helper('date');
        date_default_timezone_set("Asia/Kolkata");
    }
    

    /**********
     * Use:get all sub categories by category id
     * Method:get
     * Response : OK********* */
    public function getSubCategoriesByUser_get($CatId = "")
    {       
        $getSubCategories = $this->user_service->getSubCategoriesByUser($CatId);
        if ($getSubCategories) {
            $this->response(array("message" => MESSAGE_conf::SUCCESS, 'data' => $getSubCategories), REST_Controller::HTTP_OK);
        } else {
            $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_OK);
        }
    }
}
