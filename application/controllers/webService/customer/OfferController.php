<?php
require APPPATH . 'libraries/REST_Controller.php';
/**
 *@Author:Meenakshi
 *This controller working for request  opration
 */
class OfferController extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->config->load('myConstant');
        $this->load->library('session');
        $this->load->library('Authorization_Token');
        $this->load->helper(array('form', 'url', 'Validation_helper'));
        $this->load->library('form_validation');
        $this->load->database('');
        $this->load->service('User_service');
    }

   /******
    * Use:get all Offer by user
    * Method:Get
    * Response:ok
    ******** */
    public function getOfferByUser_get($offerId = "")
    {
        $headers = $this->input->request_headers();
        $result = tokenVerification($headers);
        if($result)
        {
            if($offerId !="")
            {
                $getOffer = $this->user_service->getAllOfferByUser();
            }
            else 
            {
                $getOffer = $this->user_service->getAllOfferById($offerId);
            }
            if($getOffer)
            {
                $this->response(array("message" => MESSAGE_conf::SUCCESS,"data"=> $getOffer), REST_Controller::HTTP_OK);
            }
            else 
            {
                $this->response(array("message" => MESSAGE_conf::FAILED), REST_Controller::HTTP_OK);
            }
        }
        else 
        {
            return $result;
        }
    }
}
